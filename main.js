const { app, BrowserWindow, Menu } = require("electron");
const url = require("url");
const path = require("path");
const kill = require("tree-kill");
var cp = require("child_process");

var configJson = require(path.join(__dirname, `/dist/config.json`));

let appWindow;
let child;

function initWindow() {
  appWindow = new BrowserWindow({
    width: configJson.standartWidth,
    height: configJson.standartHeight,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  if (configJson.startServerAutomatically) {
    startJavaServer();

    appWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, `loadingPage.html`),
        protocol: "file:",
        slashes: true,
      })
    );
  }

  // Electron Build Path
  var startDelayInMilliseconds = configJson.startServerAutomatically
    ? configJson.startDelayInSeconds * 1000
    : 0;
  setTimeout(() => {
    appWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, `/dist/index.html`),
        protocol: "file:",
        slashes: true,
      })
    );
  }, startDelayInMilliseconds);

  appWindow.on("closed", () => {
    removeChild(child);

    appWindow = null;
  });

  app.on("window-all-closed", () => {
    removeChild(child);

    // On macOS specific close process
    if (process.platform !== "darwin") {
      app.quit();
    }
  });

  app.on("closed", () => {
    removeChild(child);
  });

  app.on("activate", () => {
    if (appWindow === null) {
      initWindow();
    }
  });
}

app.on("ready", initWindow);

function removeChild(child) {
  if (configJson.startServerAutomatically && child != undefined) {
    kill(child.pid);
  }
}

function startJavaServer() {
  let pathToSharedLib = "-Djava.library.path=" + configJson.pathToCplexLibrary;

  child = cp.spawn(
    "java",
    [
      "-jar",
      path.join(__dirname, `/dist/` + configJson.server),
      pathToSharedLib,
    ],
    {
      cwd: "",
      detached: true,
      stdio: "inherit",
      silent: true,
    }
  );

  child.on("error", (err) => {
    console.log("\n\t\tERROR: spawn failed! (" + err + ")");
  });
}
