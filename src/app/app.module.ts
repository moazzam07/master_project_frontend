import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { MainCommunicationService } from './shared/services/load-data/main-communication.service';
import { PhaseOneTwoService } from './shared/services/injection/phase-one-two-injectable.service';
import { DemoMaterialModule } from './shared/utils/material-module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    FormsModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
  ],
  providers: [MainCommunicationService, PhaseOneTwoService],
  bootstrap: [AppComponent],
})
export class AppModule {}
