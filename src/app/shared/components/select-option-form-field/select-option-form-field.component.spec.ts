import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectOptionFormFieldComponent } from './select-option-form-field.component';

describe('SelectOptionFormFieldComponent', () => {
  let component: SelectOptionFormFieldComponent;
  let fixture: ComponentFixture<SelectOptionFormFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectOptionFormFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOptionFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
