import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { InfoErrorDialogComponent } from '../info-error-dialog/info-error-dialog.component';
import {
  SelectOptionModel,
  InfoErrorDialogConfiguration,
} from './../../models/models';

@Component({
  selector: 'select-option-form-field',
  templateUrl: './select-option-form-field.component.html',
  styleUrls: ['./select-option-form-field.component.scss'],
})
export class SelectOptionFormFieldComponent {
  @Input()
  public title: string;
  @Input()
  public selectFormControl: FormControl;
  @Input()
  public selectIsRequired: boolean;
  @Input()
  public selectOptions: SelectOptionModel[];

  @Input()
  private informationDialogText: string;

  constructor(private dialog: MatDialog) {}

  public openInformationDialog() {
    this.dialog.open(InfoErrorDialogComponent, {
      data: {
        isError: false,
        displayText: this.informationDialogText,
        buttonText: ['Schließen'],
        header: 'Informationen',
      } as InfoErrorDialogConfiguration,
    });
  }
}
