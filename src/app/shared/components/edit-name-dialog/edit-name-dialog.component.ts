import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { MainCommunicationService } from '../../services/load-data/main-communication.service';
import { EditNameDialogModel } from './../../models/models';

@Component({
  selector: 'edit-name-dialog',
  templateUrl: './edit-name-dialog.component.html',
  styleUrls: ['./edit-name-dialog.component.scss'],
  providers: [MainCommunicationService],
})
export class EditNameDialogComponent implements OnInit, OnDestroy {
  public name: string = ''; // Internal storage to enable abort

  private subscriptions: Subscription[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: EditNameDialogModel,
    private dialogRef: MatDialogRef<EditNameDialogComponent>,
    private mainCommunicationService: MainCommunicationService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.name = this.data.model.name;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Saves currently input name for selected model to db.
   * Notifies user upon successful save and passes back updated model.
   */
  public onSaveClick() {
    this.data.model.name = this.name;
    this.subscriptions.push(
      this.mainCommunicationService
        .updateData(this.data.urlDirectoryPath, this.data.model) // use model dependent HTTP-path
        .subscribe((response) => {
          if (response) {
            this.openSnackBar('Name geändert!', 'X');
            this.dialogRef.close(JSON.parse(response.data)); // only passses back data if saved, nothing "on close"
          }
        })
    );
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass:"mb-5"
    });
  }
}
