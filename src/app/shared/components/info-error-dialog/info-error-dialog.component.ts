import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InfoErrorDialogConfiguration } from '../../models/models';

@Component({
  selector: 'info-error-dialog',
  templateUrl: './info-error-dialog.component.html',
  styleUrls: ['./info-error-dialog.component.scss'],
})
export class InfoErrorDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<InfoErrorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InfoErrorDialogConfiguration
  ) {}

  /**
   * Determines if passed in variable is an array.
   *
   * @param {(string | string[])} variable variable to check
   * @returns {boolean} true, if array
   */
  public isVariableAnArray(variable: string | string[]): boolean {
    return Array.isArray(variable);
  }
}
