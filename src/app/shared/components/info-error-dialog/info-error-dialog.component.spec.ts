import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoErrorDialogComponent } from './info-error-dialog.component';

describe('InfoErrorDialogComponent', () => {
  let component: InfoErrorDialogComponent;
  let fixture: ComponentFixture<InfoErrorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoErrorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoErrorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
