import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { InfoErrorDialogConfiguration } from '../../models/models';
import { InfoErrorDialogComponent } from '../info-error-dialog/info-error-dialog.component';

@Component({
  selector: 'input-form-field',
  templateUrl: './input-form-field.component.html',
  styleUrls: ['./input-form-field.component.scss'],
})
export class InputFormFieldComponent {
  @Input()
  public title: string;
  @Input()
  public inputType: string;
  @Input()
  public inputPlaceholder: string;
  @Input()
  public inputFormControl: FormControl;
  @Input()
  public inputMin: string | number;
  @Input()
  public inputMax: string | number;
  @Input()
  public inputStep: string | number;
  @Input()
  public inputIsRequired: boolean;
  @Input()
  public inputSuffixText: string;
  @Input()
  public errorText: string;

  @Input()
  private informationDialogText: string;

  constructor(private dialog: MatDialog) {}

  public openInformationDialog() {
    this.dialog.open(InfoErrorDialogComponent, {
      data: {
        isError: false,
        displayText: this.informationDialogText,
        buttonText: ['Schließen'],
        header: 'Informationen',
      } as InfoErrorDialogConfiguration,
    });
  }
}
