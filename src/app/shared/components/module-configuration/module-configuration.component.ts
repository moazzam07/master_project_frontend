import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ModuleModel, SelectOptionModel } from '../../models/models';

@Component({
  selector: 'module-configuration',
  templateUrl: './module-configuration.component.html',
  styleUrls: ['./module-configuration.component.scss'],
})
export class ModuleConfigurationComponent implements OnInit, OnDestroy {
  // values expected in db
  public readonly NOT_APPLICABLE = 'na';
  public readonly THEORY = 'theory';
  public readonly PRAXIS = 'praxis';

  private readonly NO_EVENT = { emitEvent: false };

  @Input()
  private set givenModule(data: ModuleModel) {
    if (!data || !data.id) {
      // persisted data expected
      this._givenModule.next({} as ModuleModel);
      this.disableFormControls(true);
    } else {
      Object.assign(this._givenModule.value, data); // copy data to enable abort and reset
      this.disableFormControls(false);
      this.moduleConfigurationGroup.updateValueAndValidity({
        emitEvent: true,
      });
    }
  }
  @Output() private moduleConfigurationChanged = new EventEmitter<
    ModuleModel
  >();

  public moduleConfigurationGroup: FormGroup;
  public shortNameControl = new FormControl(null, Validators.minLength(1));
  public theoryPraxisTypeControl = new FormControl(null);
  public theoryPraxisTypeSelectionOptions: SelectOptionModel[] = [
    { value: this.NOT_APPLICABLE, description: 'Nicht zutreffend' },
    { value: this.THEORY, description: 'Theorie' },
    { value: this.PRAXIS, description: 'Praxis' },
  ];
  public lecturesControl = new FormControl(null, Validators.min(0));
  public exercisesControl = new FormControl(null, Validators.min(0));
  public studentsControl = new FormControl(null, Validators.min(0));

  private _givenModule = new BehaviorSubject<ModuleModel>({} as ModuleModel);
  private subscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder) {
    this.moduleConfigurationGroup = this.formBuilder.group({
      shortName: this.shortNameControl,
      theoryPraxisType: this.theoryPraxisTypeControl,
      numberOfLectures: this.lecturesControl,
      numberOfExercises: this.exercisesControl,
      numberOfStudents: this.studentsControl,
    });
  }

  ngOnInit() {
    this.disableFormControls(true);

    this.addModuleConfiugrationGroupValueChanges();
  }

  /**
   * Adds change listener to FormGroup.
   *
   * On Change & if valid:
   * Stores data in internal object.
   * Emits component change event with accordingly changed module.
   *
   */
  private addModuleConfiugrationGroupValueChanges() {
    this.moduleConfigurationGroup.valueChanges.subscribe((data) => {
      var module: ModuleModel = null;
      if (
        data &&
        this._givenModule.value.id &&
        this.moduleConfigurationGroup.valid
      ) {
        module = Object.assign({}, this._givenModule.value);
        module.shortName = data.shortName;
        module.theoryPraxisType = data.theoryPraxisType;
        module.numberOfLectures = data.numberOfLectures;
        module.numberOfExercises = data.numberOfExercises;
        module.numberOfStudents = data.numberOfStudents;
      }
      this.sendModuleConfigurationChangedEvent(module);
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Triggers update with current values from _givenModule and enabling/disabling of all FormControls of moduleConfigurationGroup.
   * Emits no event, so that change listener of FormGroup is not triggered.
   *
   * @param {boolean} disable if controls of FormGroup should be disabled
   */
  private disableFormControls(disable: boolean) {
    this.updateControlValues();

    if (disable) {
      this.moduleConfigurationGroup.disable(this.NO_EVENT);
    } else {
      this.moduleConfigurationGroup.enable(this.NO_EVENT);
    }
  }

  /**
   * Propagates all values from _givenModule to FormControls.
   * Emits no event, so that change listener of FormGroup is not triggered.
   */
  private updateControlValues() {
    this.shortNameControl.setValue(
      this._givenModule.value.shortName,
      this.NO_EVENT
    );
    const theoryPraxisType = this._givenModule.value.theoryPraxisType;
    this.theoryPraxisTypeControl.setValue(
      theoryPraxisType ? theoryPraxisType : this.NOT_APPLICABLE,
      this.NO_EVENT
    );
    this.lecturesControl.setValue(
      this._givenModule.value.numberOfLectures,
      this.NO_EVENT
    );
    this.exercisesControl.setValue(
      this._givenModule.value.numberOfExercises,
      this.NO_EVENT
    );
    this.studentsControl.setValue(
      this._givenModule.value.numberOfStudents,
      this.NO_EVENT
    );
  }

  /**
   * Emits event with provided changedModule.
   *
   * @param {ModuleModel} changedModule module to emit
   */
  private sendModuleConfigurationChangedEvent(changedModule: ModuleModel) {
    this.moduleConfigurationChanged.emit(changedModule);
  }
}
