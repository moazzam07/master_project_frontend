import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';
import { PDFExportConfigurations } from '../../models/models';

/**
 * This class uses the HTML Element reference and write it to a PDF file.
 */
@Component({
  selector: 'html-to-pdf-dialog',
  templateUrl: './html-to-pdf-dialog.component.html',
  styleUrls: ['./html-to-pdf-dialog.component.scss'],
})
export class HtmlToPdfDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) private data: PDFExportConfigurations) {}

  ngOnInit() {
    this.generatePDFForExport();
  }

  /**
   * Uses the HTML Element reference and writes it to a PDF file.
   * The ratio of the seen week-calender will be preserved and scaled to horizontal PDF-format A4.
   */
  private generatePDFForExport() {
    html2canvas(this.data.tableData).then((canvas: HTMLCanvasElement) => {
      const htmlWidth = canvas.width;
      const htmlHeight = canvas.height;
      const htmlAspectRatio = htmlWidth / htmlHeight; // defines the scaling factor

      const a4Width = 29.7;
      const a4Height = 21;
      const a4AspectRatio = a4Width / a4Height; // target ratio needed to diverge between scaling to max width and height.

      const paddingWidth = a4Width * 0.05;
      const paddingHeight = a4Height * 0.05;

      var imgWidth: number;
      var imgHeight: number;
      if (htmlAspectRatio > a4AspectRatio) {
        imgWidth = a4Width - 2 * paddingWidth;
        imgHeight = a4Width / htmlAspectRatio - 2 * paddingHeight;
      } else {
        imgWidth = a4Height * htmlAspectRatio - 2 * paddingWidth;
        imgHeight = a4Height - 2 * paddingHeight;
      }

      let pdf = new jspdf('l', 'cm', 'a4');
      pdf.addImage(
        canvas.toDataURL('image/png'),
        'PNG',
        paddingWidth,
        paddingHeight,
        imgWidth,
        imgHeight
      );
      pdf.save('Stundenplan-Export.pdf');
    });
  }
}
