import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlToPdfDialogComponent } from './html-to-pdf-dialog.component';

describe('HtmlToPdfDialogComponent', () => {
  let component: HtmlToPdfDialogComponent;
  let fixture: ComponentFixture<HtmlToPdfDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlToPdfDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlToPdfDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
