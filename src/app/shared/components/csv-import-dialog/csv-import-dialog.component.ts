import { Component, ViewChild } from '@angular/core';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { ModuleModel } from '../../models/models';

/**
 * Opens a dialog that allows the user to upload a CSV.
 */
@Component({
  selector: 'csv-import-dialog',
  templateUrl: './csv-import-dialog.component.html',
  styleUrls: ['./csv-import-dialog.component.scss'],
})
export class CsvImportDialogComponent {
  public csvRecords: ModuleModel[] = [];
  public isCsv: boolean;

  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;

  private header = true;

  constructor(private ngxCsvParser: NgxCsvParser) {}

  /**
   * This method takes the event from the uploaded CSV file and maps the result to an interface.
   *
   * @param {*} file passed from input field
   */
  public fileChangeListener(file: any) {
    const files = file.srcElement.files;

    this.ngxCsvParser
      .parse(files[0], { header: this.header, delimiter: ';' })
      .pipe()
      .subscribe(
        (result) => {
          this.csvRecords = result;
          this.isCsv = true;
        },
        (error: NgxCSVParserError) => {
          // error logging is only useful in dev mode
          // console.log('Error', error);
        }
      );
  }
}
