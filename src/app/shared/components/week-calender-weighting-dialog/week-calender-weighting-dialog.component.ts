import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WeightedTimeSlotModel } from '../../models/models';

/**
 * Opens a dialog for weight information
 */
@Component({
  selector: 'week-calender-weighting-dialog',
  templateUrl: './week-calender-weighting-dialog.component.html',
  styleUrls: ['./week-calender-weighting-dialog.component.scss'],
})
export class WeekCalenderWeightingDialogComponent implements OnInit {
  public weightingControl = new FormControl(50, [
    Validators.min(0),
    Validators.max(100),
  ]);

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: WeightedTimeSlotModel,
    private dialogRef: MatDialogRef<WeekCalenderWeightingDialogComponent>,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    if (this.data.weighting) {
      this.weightingControl.setValue(this.data.weighting);
    }
  }

  /**
   * Sets weight value directly on Timeslot.
   * Notifies user accordingly.
   */
  public onSaveClick() {
    this.data.weighting = this.weightingControl.value;
    this.openSnackBar('Gewichtung geändert!', 'X');
    this.dialogRef.close();
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }
}
