import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeekCalenderWeightingDialogComponent } from './week-calender-weighting-dialog.component';

describe('WeekCalenderWeightingDialogComponent', () => {
  let component: WeekCalenderWeightingDialogComponent;
  let fixture: ComponentFixture<WeekCalenderWeightingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeekCalenderWeightingDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekCalenderWeightingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
