import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { BehaviorSubject } from 'rxjs';
import {
  InfoErrorDialogConfiguration,
  ModuleModel,
} from 'src/app/shared/models/models';
import {
  BasicTileModel,
  BasicTimeSlotModel,
  PhaseOneSolutionModel,
  PhaseOneTileModel,
  PhaseTwoModel,
  PhaseTwoTileModel,
  ProfessorConfigurationTileModel,
  ProfessorConfigurationWeekCalenderTransitionModel,
  WeekCalenderConfigurationModel,
  WeightedTimeSlotModel,
} from '../../models/models';
import { InfoErrorDialogComponent } from '../info-error-dialog/info-error-dialog.component';
import { WeekCalenderWeightingDialogComponent } from '../week-calender-weighting-dialog/week-calender-weighting-dialog.component';
import {
  PhaseTwoTimeslotGroupModuleModel,
  TimeSlotGroupModel,
} from './../../models/models';

@Component({
  selector: 'week-calender',
  templateUrl: './week-calender.component.html',
  styleUrls: ['./week-calender.component.scss'],
  providers: [MatMenuTrigger],
})
export class WeekCalenderComponent {
  public readonly rowsDefinition: WeekCalenderConfigurationModel = {
    amount: 5,
    lables: ['08-10 Uhr', '10-12 Uhr', '12-14 Uhr', '14-16 Uhr', '16-18 Uhr'],
  };
  public readonly columnsDefinition: WeekCalenderConfigurationModel = {
    amount: 5,
    lables: ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag'],
  };

  private readonly blockedSlots: BasicTimeSlotModel[] = [
    {
      day: 2,
      time: 3,
    },
    {
      day: 2,
      time: 4,
    },
  ];
  private readonly idsOfBlockedTimeSlots: number[] = this.blockedSlots.map(
    (element) => this.calculateTileId(element)
  );
  private readonly totalNumberOfTiles: number =
    this.columnsDefinition.amount * this.rowsDefinition.amount;

  @Input()
  public set mode(mode: string) {
    if (
      mode === 'professor-configuration' ||
      mode === 'phase-one' ||
      mode === 'phase-two'
    ) {
      this._mode = mode;
    }
  }
  public get mode() {
    return this._mode;
  }
  @Input()
  private set givenAvailability(
    data: ProfessorConfigurationWeekCalenderTransitionModel
  ) {
    this.tileList = [];
    this._givenAvailability.next(data);
    this.professorConfiguration_createTiles();
  }
  @Input()
  private set givenPhaseOneSolution(data: PhaseOneSolutionModel) {
    this.tileList = [];
    this._givenPhaseOneSolution.next(data);
    this.phaseOne_createTiles();
  }

  @Input() private set givenPhaseTwoSolution(data: PhaseTwoModel) {
    this.tileList = [];
    this._givenPhaseTwoSolution.next(data);
    this.phaseTwo_createTiles();
  }

  @Output() private onAvailabilityChange = new EventEmitter<
    ProfessorConfigurationWeekCalenderTransitionModel
  >();

  public tileList: BasicTileModel[] = [];

  private _mode: 'professor-configuration' | 'phase-one' | 'phase-two';
  private _givenAvailability = new BehaviorSubject<
    ProfessorConfigurationWeekCalenderTransitionModel
  >(null);
  private _givenPhaseOneSolution = new BehaviorSubject<PhaseOneSolutionModel>(
    null
  );
  private _givenPhaseTwoSolution = new BehaviorSubject<PhaseTwoModel>(null);

  constructor(private dialog: MatDialog) {}

  // ***********************************************************************************************
  // Shared
  // ***********************************************************************************************
  /**
   * Calculates the id of a tile to be displayed in order in the week-calender by day & time value.
   *
   * @param {BasicTimeSlotModel} timeslot timeslot to calculate id for
   * @returns {number} if of timeslot
   */
  private calculateTileId(timeslot: BasicTimeSlotModel): number {
    var rowMultiplier: number = this.columnsDefinition.amount * timeslot.time;
    return rowMultiplier + timeslot.day;
  }

  /**
   * Calculates the id day & time value of timeslot by the id in the week-calender.
   *
   * @param {number} index id to calculate day & time for
   * @returns {BasicTimeSlotModel} timeslot with day & time
   */
  private calculateDayAndTime(index: number): BasicTimeSlotModel {
    var day: number = index % this.columnsDefinition.amount;
    var time: number = Math.floor(index / this.columnsDefinition.amount);
    return {
      day: day,
      time: time,
    } as BasicTimeSlotModel;
  }

  /**
   * Returns a set of seemingly distinct html colors.
   *
   * @returns {string[]} array of strings with html colors
   */
  private getDistinctColors(numberOfColors: number): string[] {
    // Not distinct enough :/
    // const colors: string[] = [];
    // for (let i = 0; i < 360; i += 360 / numberOfColors) {
    //   colors.push("hsl(" + i + ',100%,50%)');
    // }
    // return colors;

    // 25 colors generated with: https://mokole.com/palette.html
    // First ones being less distinct, so they will probably not be used
    const colors: string[] = [
      'yellowgreen',
      'deepskyblue',
      'deeppink',
      'lawngreen',

      'darkslategray',
      'darkolivegreen',
      'green',
      'darkmagenta',
      'turquoise',
      'blueviolet',
      'blue',
      'coral',
      'fuchsia',
      'khaki',
      'violet',
      'palevioletred',

      'mediumspringgreen',
      'saddlebrown',
      'darkslateblue',
      'lightgray',
      'dodgerblue',
      'navy',
      'red',
      'orange',
      'yellow',
    ];
    return colors.slice(colors.length - 1 - numberOfColors, colors.length - 1);
  }

  // ***********************************************************************************************
  // Professor-Configuration
  // ***********************************************************************************************
  /**
   * Adds tiles for every found availability on the professor and adds it to the tileList.
   * BlockedTimeSlots will be marked accordingly. Rest of tiles if not present are filled up (also for initialization).
   * Sorts tileList by id.
   */
  private professorConfiguration_createTiles() {
    if (this._givenAvailability.getValue()) {
      this.professorConfiguration_mapPredefinedTiles();
    }

    this.professorConfiguration_fillMissingTiles();

    this.sortTileListAscending();
  }

  /**
   * Sorts tileList ascending by id.
   */
  private sortTileListAscending() {
    this.tileList.sort((a, b) => a.id - b.id);
  }

  /**
   * Adds for each type of availability (available, possible, unavailable) an according tile to the tileList.
   */
  private professorConfiguration_mapPredefinedTiles() {
    this._givenAvailability
      .getValue()
      .availableTimeslots?.forEach((timeSlot) => {
        this.professorConfiguration_addNewTileWithSpecificId(
          timeSlot,
          'available'
        );
      });

    this._givenAvailability
      .getValue()
      .possibleTimeslots?.forEach((timeSlot) => {
        this.professorConfiguration_addNewTileWithSpecificId(
          timeSlot as WeightedTimeSlotModel,
          'possible'
        );
      });

    this._givenAvailability
      .getValue()
      .unavailableTimeslots?.forEach((timeSlot) => {
        this.professorConfiguration_addNewTileWithSpecificId(
          timeSlot,
          'unavailable'
        );
      });
  }

  /**
   * Adds a tile with specific information to the tileList.
   *
   * @param {BasicTimeSlotModel} timeSlot timeslot to add tile for
   * @param {('available' | 'possible' | 'unavailable')} state availability state of tile
   */
  private professorConfiguration_addNewTileWithSpecificId(
    timeSlot: BasicTimeSlotModel,
    state: 'available' | 'possible' | 'unavailable'
  ) {
    const id = this.calculateTileId(timeSlot);
    this.professorConfiguration_addNewTile(id, timeSlot, state);
  }

  /**
   * Adds an available default tile for each missing tile in tileList (determined by missing ids (/ day & time) combinations).
   */
  private professorConfiguration_fillMissingTiles() {
    for (var i = 0; i < this.totalNumberOfTiles; i++) {
      if (!this.tileList.some((tile) => tile.id == i)) {
        this.professorConfiguration_addNewTile(
          i,
          this.calculateDayAndTime(i),
          'available'
        );
      }
    }
  }

  /**
   * Adds a tile with specific information to the tileList.
   *
   * @param {number[]} idsOfBlockedTimeSlots  ids of blocked Timeslots
   * @param {number} id if od tile
   * @param {BasicTimeSlotModel} timeSlot timeslot to add tile for
   * @param {('available' | 'possible' | 'unavailable')} state availability state
   */
  private professorConfiguration_addNewTile(
    id: number,
    timeSlot: BasicTimeSlotModel,
    state: 'available' | 'possible' | 'unavailable'
  ) {
    this.tileList.push({
      id: id,
      timeSlot: timeSlot,
      state: state,
      isBlocked: this.idsOfBlockedTimeSlots.includes(id),
      isCurrentSelection: false,
    } as ProfessorConfigurationTileModel);
  }

  /**
   * Sets isCurrentSelection to true for selected tile.
   * Resets all others.
   *
   * @param {ProfessorConfigurationTileModel} tile to set seleciton state for
   */
  public toggleIsOpenState(tile: ProfessorConfigurationTileModel) {
    this.tileList.forEach((tile: ProfessorConfigurationTileModel) => {
      tile.isCurrentSelection = false;
    });
    tile.isCurrentSelection = true;
  }

  /**
   * Propagated state change / state selection to chosen tile.
   * Emits change event.
   *
   * @param {ProfessorConfigurationTileModel} tile tile to handle state change for
   * @param {('available' | 'possible' | 'unavailable')} newState  new availability state
   */
  public handleTileState(
    tile: ProfessorConfigurationTileModel,
    newState: 'available' | 'possible' | 'unavailable'
  ) {
    tile.state = newState;
    this.emitOnAvailabilityChange();
  }

  /**
   * Handles state change / state setting for all tiles in tileList.
   * Emits change event.
   *
   * @param {('allAvailable' | 'allPossible' | 'allUnavailable')} action default state to propagate
   */
  public handleNavBarAction(
    action: 'allAvailable' | 'allPossible' | 'allUnavailable'
  ) {
    this.tileList.forEach((tile: ProfessorConfigurationTileModel) => {
      if (tile.isBlocked == false) {
        switch (action) {
          case 'allAvailable':
            tile.state = 'available';
            break;

          case 'allPossible':
            tile.state = 'possible';
            break;

          case 'allUnavailable':
          default:
            tile.state = 'unavailable';
            break;
        }
      }
    });

    this.emitOnAvailabilityChange();
  }

  /**
   * Creates calender export from currently chosen availability and emits it.
   */
  private emitOnAvailabilityChange() {
    var calenderExport = {
      availableTimeslots: [],
      possibleTimeslots: [],
      unavailableTimeslots: [],
    };

    this.tileList.forEach((tile: ProfessorConfigurationTileModel) =>
      this.addTimeslotOfTileToCalenderExport(tile, calenderExport)
    );

    this.onAvailabilityChange.emit(calenderExport);
  }

  /**
   * Maps tile by state to according list in calenderExport.
   *
   * @param {ProfessorConfigurationTileModel} tile to map to according list
   * @param {ProfessorConfigurationWeekCalenderTransitionModel} calenderExport export object containing all state lists
   */
  private addTimeslotOfTileToCalenderExport(
    tile: ProfessorConfigurationTileModel,
    calenderExport: ProfessorConfigurationWeekCalenderTransitionModel
  ) {
    switch (tile.state) {
      case 'available':
        calenderExport.availableTimeslots.push(
          this.mapToBasicTimeSlotModel(tile)
        );
        break;

      case 'possible':
        calenderExport.possibleTimeslots.push(
          this.mapToWeightedTimeSlotModel(tile)
        );
        break;

      case 'unavailable':
      default:
        calenderExport.unavailableTimeslots.push(
          this.mapToBasicTimeSlotModel(tile)
        );
        break;
    }
  }

  /**
   * Maps tile to basic timeslot to get rid of possible weighting after availabilty change to non-"possible".
   *
   * @param {ProfessorConfigurationTileModel} tile tile to map
   * @returns {BasicTimeSlotModel} mapped timeslot
   */
  private mapToBasicTimeSlotModel(
    tile: ProfessorConfigurationTileModel
  ): BasicTimeSlotModel {
    return {
      day: tile.timeSlot.day,
      time: tile.timeSlot.time,
    };
  }

  /**
   * Maps tile to weighted timeslot, to preserve possible weighting.
   * Defaults if not available.
   *
   * @param {ProfessorConfigurationTileModel} tile to map
   * @returns {WeightedTimeSlotModel} mapped weighted timeslot
   */
  private mapToWeightedTimeSlotModel(
    tile: ProfessorConfigurationTileModel
  ): WeightedTimeSlotModel {
    const weightedTimeSlot = tile.timeSlot as WeightedTimeSlotModel;
    return {
      day: weightedTimeSlot.day,
      time: weightedTimeSlot.time,
      weighting: weightedTimeSlot.weighting ? weightedTimeSlot.weighting : 50,
    };
  }

  public openWeightDialog(timeslot: WeightedTimeSlotModel) {
    this.dialog.open(WeekCalenderWeightingDialogComponent, {
      data: timeslot,
    });
  }

  // ***********************************************************************************************
  // phase 1
  // ***********************************************************************************************
  /**
   * Creates tiles for every found timeslot of every timeslotGroup and adds it to the tileList.
   * BlockedTimeSlots will be marked accordingly. Rest of tiles if not present are filled up (also for initialization).
   * Sorts tileList by id.
   */
  private phaseOne_createTiles() {
    this.phaseOne_mapGivenTimeSlotGroups();
    this.phaseOne_fillMissingTiles();
    this.sortTileListAscending();
  }

  /**
   * Creates tiles for every found timeslot of every timeslotGroup and adds it to the tileList.
   */
  private phaseOne_mapGivenTimeSlotGroups() {
    const numberOfTimeSlotGroups = this._givenPhaseOneSolution.getValue()
      ?.timeSlotGroups?.length;
    const colors: string[] = this.getDistinctColors(numberOfTimeSlotGroups);

    this._givenPhaseOneSolution
      .getValue()
      ?.timeSlotGroups?.forEach((group) =>
        this.phaseOne_createTilesForTimeSlotGroup(group, colors)
      );
  }

  /**
   * Adds an empty default tile for each missing tile in tileList (determined by missing ids (/ day & time) combinations).
   */
  private phaseOne_fillMissingTiles() {
    for (var i = 0; i < this.totalNumberOfTiles; i++) {
      if (!this.tileList.some((tile) => tile.id == i)) {
        this.phaseOne_addNewTile(i, null, 'whitesmoke', null);
      }
    }
  }

  /**
   * Creates tile for each timeslot in group and adds it with group specific color to tileList.
   *
   * @param {TimeSlotGroupModel} group group to add tile for
   * @param {string[]} colors colors to chose for group
   */
  private phaseOne_createTilesForTimeSlotGroup(
    group: TimeSlotGroupModel,
    colors: string[]
  ) {
    const groupColor = colors.pop();

    group.timeSlots.forEach((timeSlot) => {
      const id = this.calculateTileId(timeSlot);
      this.phaseOne_addNewTile(id, group, groupColor, timeSlot);
    });
  }

  /**
   * Adds a tile with specific information to the tileList.
   *
   * @param {number[]} idsOfBlockedTimeSlots  ids of blocked Timeslots
   * @param {number} id if od tile
   * @param {TimeSlotGroupModel} timeSlotGroup timeslotGroup to add to tile
   * @param {string} color color to add for timeSlotGroup
   * @param {BasicTimeSlotModel} timeSlot timeslot to add tile for
   */
  private phaseOne_addNewTile(
    id: number,
    timeSlotGroup: TimeSlotGroupModel,
    color: string,
    timeSlot: BasicTimeSlotModel
  ) {
    this.tileList.push({
      id: id,
      timeSlot: timeSlot,
      isBlocked: this.idsOfBlockedTimeSlots.includes(id),
      timeSlotGroup: timeSlotGroup,
      color: color,
    } as PhaseOneTileModel);
  }

  // ***********************************************************************************************
  // phase 2
  // ***********************************************************************************************
  /**
   * Adds tiles for every found timeslot of every timeslotGroup with modules and adds it to the tileList.
   * BlockedTimeSlots will be marked accordingly. Rest of tiles if not present are filled up (also for initialization).
   * Sorts tileList by id.
   */
  private phaseTwo_createTiles() {
    this.phaseTwo_mapGivenTimeSlotGroups();
    this.phaseTwo_fillMissingTiles();
    this.sortTileListAscending();
  }
  
  /**
   * Creates tiles for every found timeslot of every timeslotGroup and adds it to the tileList.
   */
  private phaseTwo_mapGivenTimeSlotGroups() {
    const numberOfTimeSlotGroups = this._givenPhaseTwoSolution.getValue()
      ?.groupedModules?.length;
    const colors: string[] = this.getDistinctColors(numberOfTimeSlotGroups);

    this._givenPhaseTwoSolution
      .getValue()
      ?.groupedModules?.forEach((timeslotGroupModuleModel) =>
        this.phaseTwo_createTilesForTimeSlotGroup(
          timeslotGroupModuleModel,
          colors
        )
      );
  }

  /**
   * Adds an empty default tile for each missing tile in tileList (determined by missing ids (/ day & time) combinations).
   */
  private phaseTwo_fillMissingTiles() {
    for (var i = 0; i < this.totalNumberOfTiles; i++) {
      if (!this.tileList.some((tile) => tile.id == i)) {
        this.phaseTwo_addNewTile(i, null, null, 'whitesmoke', null);
      }
    }
  }

  /**
   * Creates tile for each timeslot in group and adds it with group specific color to the tileList.
   *
   * @param {TimeSlotGroupModel} group group to add tile for
   * @param {string[]} colors colors to chose for group
   */
  private phaseTwo_createTilesForTimeSlotGroup(
    group: PhaseTwoTimeslotGroupModuleModel,
    colors: string[]
  ) {
    const groupColor = colors.pop();

    var timeSlotGroup: TimeSlotGroupModel = group?.timeslotGroup;

    timeSlotGroup?.timeSlots?.forEach((timeSlot) => {
      const id = this.calculateTileId(timeSlot);
      this.phaseTwo_addNewTile(
        id,
        timeSlotGroup,
        group?.modules,
        groupColor,
        timeSlot
      );
    });
  }

  /**
   *
   * Adds a tile with specific information to the tileList.
   *
   * @param {number[]} idsOfBlockedTimeSlots  ids of blocked Timeslots
   * @param {number} id if od tile
   * @param {TimeSlotGroupModel} timeSlotGroup timeslotGroup to add to tile
   * @param {ModuleModel[]} modules modules to add to tile
   * @param {string} color color to add for timeSlotGroup
   * @param {BasicTimeSlotModel} timeSlot timeslot to add tile for
   */
  private phaseTwo_addNewTile(
    id: number,
    timeSlotGroup: TimeSlotGroupModel,
    modules: ModuleModel[],
    color: string,
    timeSlot: BasicTimeSlotModel
  ) {
    this.tileList.push({
      id: id,
      timeSlot: timeSlot,
      isBlocked: this.idsOfBlockedTimeSlots.includes(id),
      timeSlotGroup: timeSlotGroup,
      modules: modules,
      color: color,
    } as PhaseTwoTileModel);
  }

  public openInfoDialog(displayText: string) {
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText: displayText,
      buttonText: ['Schließen'],
      header: 'Informationen',
    };
    this.dialog.open(InfoErrorDialogComponent, {
      width: '50%',
      maxHeight: '75%',
      data: dialogConfig,
    });
  }

  /**
   * Future work: Propagate dragging of module to TimeslotGroup.
   *
   * Problems:
   * - When one module is dragged onto one tile, what happens to the rest of the references? Will they be set to the new TimeslotGroup of the tile or stay?
   * - How are now arising conflicts displayed and handled to/by the user.
   *
   * Result:
   * - Future work that has to thoroughly be investigated after initial use. Maybe there is no need for this as user just takes the result as a hint
   *   and drags single modules around with according teacher on hand.
   *
   * @param {ModuleModel} draggedModule module that has been dragged
   * @param {BasicTileModel} tile tile that module has been dragged on
   */
  public dragAndDropModel(draggedModule: ModuleModel, tile: BasicTileModel) {}
}
