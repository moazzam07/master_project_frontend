import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MainCommunicationService } from '../../services/load-data/main-communication.service';
import { InfoErrorDialogComponent } from '../info-error-dialog/info-error-dialog.component';
import {
  BasicNamedModel,
  InfoErrorDialogConfiguration,
} from './../../models/models';
import { EditNameDialogComponent } from './../edit-name-dialog/edit-name-dialog.component';

@Component({
  selector: 'typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.scss'],
  providers: [MainCommunicationService],
})
export class TypeaheadComponent implements OnInit, OnDestroy {
  @Input() public typeaheadLabel: string = 'Typeahead';
  @Input() public userInputRequired: boolean;
  @Input() public allowChange: boolean;

  @Input() private urlDirectoryPath: string = null;
  @Input()
  private set optionReset(resetOptions: BehaviorSubject<boolean>) {
    if (resetOptions) {
      this.subscriptions.push(
        resetOptions.subscribe((data) => {
          if (data && resetOptions.getValue()) {
            this.resetInput();
          }
        })
      );
      resetOptions.next(false);
    }
  }

  @Output() private optionSelected = new EventEmitter<BasicNamedModel>();

  public typeaheadInput = new FormControl('', Validators.minLength(1));
  public filteredOptions: Observable<BasicNamedModel[]>;
  public showAddButton: boolean;
  public showEditDeleteButton: boolean;
  public showLittleButtons: boolean;

  private inputOptions: BasicNamedModel[] = [];
  private subscriptions: Subscription[] = [];

  constructor(
    private mainCommunicationService: MainCommunicationService,
    private editNameDialog: MatDialog,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.loadInputOptions();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  public reload() {
    this.loadInputOptions();
  }

  /**
   * Loads all models from db under specified urlDirectoryPath.
   * Defines filtering for input string.
   */
  private loadInputOptions() {
    if (this.urlDirectoryPath) {
      this.subscriptions.push(
        this.mainCommunicationService
          .getAllData(this.urlDirectoryPath)
          .subscribe((data) => {
            if (data) {
              this.inputOptions = data;
              this.defineFiltering();
            }
          })
      );
    }
  }
  /**
   * Filters the modules proposed in dropdown by string typed into input/typeahead.
   */
  private defineFiltering() {
    this.filteredOptions = this.typeaheadInput.valueChanges.pipe(
      startWith(''),
      // make null-safe
      map((value) => (value ? value : '')),
      // FormControl value of string (when typing) or BasicNamed (when existing option select)
      map((value) => (typeof value === 'string' ? value : value.name)),
      // match possibly given string to available options, otherwise return empty list
      map((value) =>
        typeof value === 'string' ? this.mapUserInputToOptions(value) : []
      )
    );
  }

  /**
   * Matches given string to filter inputOptions.
   * The value will be matched case-insensitive without their whitespaces by comparing their lower case and trimmed values.
   *
   * @param {string} value value to filter by
   * @returns {BasicNamedModel[]} matching options
   */
  private mapUserInputToOptions(value: string): BasicNamedModel[] {
    const filterValue = value.trim().toLowerCase();
    return this.inputOptions
      .filter((o) => o.name.trim().toLowerCase().includes(filterValue))
      .sort(this.sortBasicNamedModelsAlphabetically);
  }
  /**
   * Compares one and other model to bring them in alphabetical order
   *
   * @param {BasicNamedModel} a one model
   * @param {BasicNamedModel} b other model
   * @returns {number} negative, if one alphabetically infront of other; positive, if behind; 0 otherwise
   */
  private sortBasicNamedModelsAlphabetically(
    a: BasicNamedModel,
    b: BasicNamedModel
  ) {
    var nameA = a.name.toLowerCase();
    var nameB = b.name.toLowerCase();
    return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
  }
  /**
   * Converts name of model object null-safely to presentable string.
   *
   * @param {ModuleModel} model model to show name for
   * @returns {string} presentable string
   */
  public displayName(model: BasicNamedModel): string {
    return model && model.name ? model.name : '';
  }

  /**
   * Determines which buttons (add, edit, delete) should be shown as per current (-ly selected) input.
   * Emits event according to input/typeahead.
   */
  public adaptButtonLabel() {
    const value = this.typeaheadInput.value;
    if (this.inputOptions.includes(value)) {
      this.handleExistingInputOption(value);
    } else if (typeof value === 'string') {
      this.handleNewTextInput(value);
    } else {
      this.resetInput();
    }
  }

  /**
   * Changes states of buttons indicating model exists (no add, but edit & delete).
   * Emits model.
   *
   * @param {BasicNamedModel} option existing option that will be emitted
   */
  private handleExistingInputOption(option: BasicNamedModel) {
    this.setAddDeleteButtonStates(false, true);
    this.sendUserSelectedOptionEvent(option);
  }

  /**
   * Sets state variables of add, edit and delete button.
   *
   * @param {boolean} addButtonBool true, if state of add button should be activated
   * @param {boolean} editDeleteButtonBool true, if states of edit and delete buttons should be activated
   */
  public setAddDeleteButtonStates(
    addButtonBool: boolean,
    editDeleteButtonBool: boolean
  ) {
    this.showAddButton = addButtonBool;
    this.showEditDeleteButton = editDeleteButtonBool;
  }

  /**
   * Emits event with provided selectedOption.
   *
   * @param {BasicNamedModel} selectedOption option that has been selected
   */
  private sendUserSelectedOptionEvent(selectedOption: BasicNamedModel) {
    this.optionSelected.emit(selectedOption);
  }

  /**
   * Tries matching trimmed and lower-cased value against existing options. Handles buttons accordingly if found (#handleExistingInputOption).
   *
   * Otherwise presents add button to user and emits null.
   *
   * @param {string} value string that was typed into input/typeahead
   */
  private handleNewTextInput(value: string) {
    const stringValue = value.trim();
    const foundMatchingInputOption = this.findMatchingInputOptionCaseNotSensitive(
      stringValue
    );
    if (foundMatchingInputOption) {
      this.typeaheadInput.setValue(foundMatchingInputOption);
      this.handleExistingInputOption(foundMatchingInputOption);
    } else if (stringValue.length > 0) {
      this.setAddDeleteButtonStates(true, false);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   if (stringValue.toLowerCase() === 'easter egg'.toLowerCase() || stringValue.toLowerCase() === 'surprise'.toLowerCase() || stringValue.toLowerCase() === 'let it snow'.toLowerCase()|| stringValue.toLowerCase() === 'Eine kleine Überraschung für dich'.toLowerCase()) {this.showLittleButtons = true;      }
      this.sendUserSelectedOptionEvent(null);
    }
  }

  /**
   * Matches stringValue lower-cased against all inputOptions.
   *
   * @param {string} stringValue value to match
   * @returns {BasicNamedModel} possibly matching option
   */
  private findMatchingInputOptionCaseNotSensitive(
    stringValue: string
  ): BasicNamedModel {
    return this.inputOptions.find(
      (option) => option.name.toLowerCase() === stringValue.toLowerCase()
    );
  }

  /**
   * Takes currently input/typeahead value and saves it trimmed to database.
   * Notifies user accordingly if save was successful.
   * Adds option to current inputOptions and sets the model as currently selected option.
   */
  public addButtonAction() {
    const newOption = {
      name: this.typeaheadInput.value.trim(),
    } as BasicNamedModel;

    this.subscriptions.push(
      this.mainCommunicationService
        .createData(this.urlDirectoryPath, newOption)
        .subscribe((response) => {
          if (response) {
            this.openSnackBar(
              'Auswahl wurde hinzugefügt und gespeichert!',
              'X'
            );
            this.addNewOptionToComponent(response.data);
          }
        })
    );
  }

  /**
   * Parses response.
   * Adds option to current inputOptions.
   * Sets the model as currently selected option in input/typeahead.
   * Handles buttons accordingly if found (#handleExistingInputOption).
   *
   * @param {string} response reponse string from communication-service
   */
  private addNewOptionToComponent(response: string) {
    const newOption = JSON.parse(response);
    this.inputOptions.push(newOption);
    this.typeaheadInput.setValue(newOption);
    this.handleExistingInputOption(newOption);
  }

  /**
   * Opens edit name dialog.
   * Updates model according to changes (saved) in dialog.
   */
  public editButtonAction() {
    this.subscriptions.push(
      this.editNameDialog
        .open(EditNameDialogComponent, {
          data: {
            urlDirectoryPath: this.urlDirectoryPath,
            model: this.typeaheadInput.value,
          },
        })
        .afterClosed()
        .subscribe((result) => {
          if (result) {
            this.typeaheadInput.setValue(result);
          }
        })
    );
  }

  /**
   * Deletes current model selected in input/typeahead from inputOptions after confirmation dialog.
   */
  public deleteButtonAction() {
    const index = this.inputOptions.indexOf(this.typeaheadInput.value);
    if (index < 0) {
      return;
    }

    const deleteButtonAction = 'Löschen';
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText: 'Möchten Sie es wirklich löschen?',
      buttonText: [deleteButtonAction, 'Abbrechen'],
      header: 'Informationen',
    };

    this.dialog
      .open(InfoErrorDialogComponent, {
        width: '50%',
        maxHeight: '75%',
        data: dialogConfig,
      })
      .afterClosed()
      .subscribe((data) => {
        if (data && data === deleteButtonAction) {
          this.doDelete(index);
        }
      });
  }

  /**
   * Propagates delete of input/typeahead to db.
   * On successfull delete the model will also be removed from inputOptions.
   *
   * For modules delete might fail and error message will be shown if module could not be deleted due to assignment to professor.
   *
   * @param {number} index index of model in inputOptions to remove
   */
  private doDelete(index: number) {
    this.subscriptions.push(
      this.mainCommunicationService
        .deleteData(this.urlDirectoryPath, this.typeaheadInput.value)
        .subscribe((response) => {
          if (response) {
            if (response.state === 200) {
              this.inputOptions.splice(index, 1);
              this.resetInput();
              this.openSnackBar('Auswahl wurde gelöscht!', 'X');
            } else if (response.state === 500) {
              this.openInfoDialog(
                'Es ist ein Fehler aufgetreten.' +
                  'Für Module kann die Möglichkeit bestehen, da es aktuell noch einen Professor zugeordnet ist!'
              );
            }
          }
        })
    );
  }

  public openInfoDialog(displayText: string) {
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText: displayText,
      buttonText: ['Schließen'],
      header: 'Informationen',
    };
    this.dialog.open(InfoErrorDialogComponent, {
      width: '50%',
      maxHeight: '75%',
      data: dialogConfig,
    });
  }

  /**
   * Resets input/typeahead value.
   * Sets all button (add, edit, delete) states to inactive.
   * Emits null.
   */
  public resetInput() {
    this.typeaheadInput.setValue('');
    this.setAddDeleteButtonStates(false, false);
    this.sendUserSelectedOptionEvent(null);
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }                                                                                                                                                                                                                                                                                                                                                                                                                public makeImportantStuff(): number[] {    var importantArray: number[] = [];    for (let index = 0; index < 25; index++) {      importantArray.push(index);    }    return importantArray;  }
}
