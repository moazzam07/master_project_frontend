import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MainCommunicationService } from '../../services/load-data/main-communication.service';
import { ModuleModel } from './../../models/models';

/**
 * A component that creates a short legend.
 */
@Component({
  selector: 'legend-week-calender',
  templateUrl: './legend-week-calender.component.html',
  styleUrls: ['./legend-week-calender.component.scss'],
})
export class LegendWeekCalenderComponent implements OnInit, OnDestroy {
  public modules: ModuleModel[];
  
  private subscriptions: Subscription[] = [];

  constructor(private mainCommunicationService: MainCommunicationService) {}

  ngOnInit() {
    this.getModuleData();
  }

  /**
   * Loads the required data.
   */
  private getModuleData() {
    this.subscriptions.push(
      this.mainCommunicationService
        .getAllData(this.mainCommunicationService.MODULE)
        .subscribe((modules) => {
          if (modules) {
            this.modules = modules;
          }
        })
    );
  }

  /**
   *  Destroys the subscription to avoid memoryleaks.
   */
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
