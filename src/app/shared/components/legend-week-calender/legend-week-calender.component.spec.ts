import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegendWeekCalenderComponent } from './legend-week-calender.component';

describe('LegendWeekCalenderComponent', () => {
  let component: LegendWeekCalenderComponent;
  let fixture: ComponentFixture<LegendWeekCalenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegendWeekCalenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegendWeekCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
