import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { ModuleModel } from '../../models/models';
import { MainCommunicationService } from '../../services/load-data/main-communication.service';

@Component({
  selector: 'module-configuration-dialog',
  templateUrl: './module-configuration-dialog.component.html',
  styleUrls: ['./module-configuration-dialog.component.scss'],
})
export class ModuleConfigurationDialogComponent implements OnInit, OnDestroy {
  public selectedModule: ModuleModel = {} as ModuleModel;
  public selectedModuleValid: boolean;

  private subscriptions: Subscription[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: ModuleModel,
    private dialogRef: MatDialogRef<ModuleConfigurationDialogComponent>,
    private mainCommunicationService: MainCommunicationService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.selectedModule = this.data;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Saves the passed in module to the db.
   *
   * Notifies user accordingly on successful update.
   */
  public onSaveClick() {
    if (this.selectedModuleValid) {
      this.subscriptions.push(
        this.mainCommunicationService
          .updateData(this.mainCommunicationService.MODULE, this.selectedModule)
          .subscribe((response) => {
            if (response) {
              this.openSnackBar('Modul bearbeitet!', 'X');
              this.dialogRef.close(JSON.parse(response.data));
            }
          })
      );
    }
  }

  /**
   * Propagates module changes passed from module-configuration component.
   *
   * @param {ModuleModel} adaptedModule module with changes to propagate
   */
  public moduleConfigurationChanged(adaptedModule: ModuleModel) {
    // module configuration component will return null if inputs not valid (or no module selected)
    if (adaptedModule) {
      this.selectedModule = adaptedModule; // changes in module-configuration component will be stored on individual object to enable abort
      this.selectedModuleValid = true;
    } else {
      this.selectedModuleValid = false;
    }
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }
}
