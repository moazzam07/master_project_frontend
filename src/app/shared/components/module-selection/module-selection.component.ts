import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MainCommunicationService } from '../../services/load-data/main-communication.service';
import { ModuleConfigurationDialogComponent } from '../module-configuration-dialog/module-configuration-dialog.component';
import { ModuleModel } from './../../models/models';

@Component({
  selector: 'module-selection',
  templateUrl: './module-selection.component.html',
  styleUrls: ['./module-selection.component.scss'],
})
export class ModuleSelectionComponent implements OnInit, OnDestroy {
  @Input() private considerAssignedModules: boolean = true;
  @Input() public disableEdit: boolean;
  @Input()
  private set givenModules(data: ModuleModel[]) {
    if (data) {
      this.loadModules();
      this.selectedModules.next(data.map((x) => x)); // clone array to prevent changes on professor
      this.reload();
    } else {
      this.selectedModules.next([]);
      this.reload();
    }
  }

  @Output() private moduleSelectionChanged = new EventEmitter<ModuleModel[]>();

  @ViewChild('moduleInput') private moduleInput: ElementRef<HTMLInputElement>;

  public separatorKeysCodes: number[] = [ENTER, COMMA];
  public moduleSelectionInput = new FormControl();
  public fitleredModules: Observable<ModuleModel[]>;
  public selectedModules = new BehaviorSubject<ModuleModel[]>([]);

  private allModules: ModuleModel[] = [];
  private subscriptions: Subscription[] = [];

  constructor(
    private mainCommunicationService: MainCommunicationService,
    private moduleConfigurationDialogComponent: MatDialog
  ) {}

  ngOnInit() {
    this.loadModules();
  }

  public reload() {
    this.loadModules();
    this.resetInput();
  }

  /**
   * Loads current data from db depending on value of considerAssignedModules
   */
  private loadModules() {
    const behaviourSubject = this.considerAssignedModules
      ? this.mainCommunicationService.getAllData(
          this.mainCommunicationService.MODULE
        )
      : this.mainCommunicationService.getSpecificData(
          this.mainCommunicationService.MODULE,
          this.mainCommunicationService.UNASSIGNED
        );
    this.subscriptions.push(
      behaviourSubject.subscribe((data: ModuleModel[]) => {
        if (data) {
          this.allModules = data;
          this.defineFiltering();
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Filters the modules proposed in dropdown by string typed into input.
   */
  private defineFiltering() {
    this.fitleredModules = this.moduleSelectionInput.valueChanges.pipe(
      startWith(''),
      // make null-safe
      map((value) => (value ? value : '')),
      // FormControl value of string (when typing) or BasicNamed-/ModuleModel (when existing module select)
      map((value) => (typeof value === 'string' ? value : value.name)),
      // match possibly given string to available options, otherwise return empty list
      map((value) =>
        typeof value === 'string' ? this.mapUserInputToOptions(value) : []
      )
    );
  }

  /**
   * Matches given string to filter allModules that are not already in selectedModules.
   * The value will be matched case-insensitive without their whitespaces by comparing their lower case and trimmed values.
   *
   * @param {string} value value to filter by
   * @returns {ModuleModel[]} matching modules
   */
  private mapUserInputToOptions(value: string): ModuleModel[] {
    const filterValue = value?.trim().toLowerCase();
    return this.allModules
      .filter((m) => !this.idContainedInSelectedModules(m.id))
      .filter((m) => m.name?.trim().toLowerCase().includes(filterValue))
      .sort(this.sortModuleModelsAlphabetically);
  }

  /**
   * Checks given id already present in selectedModules.
   *
   * @param {number} id id to check for
   * @returns {boolean} true, if some id was equal
   */
  private idContainedInSelectedModules(id: number): boolean {
    return this.selectedModules
      .getValue()
      .some((selection) => selection.id === id);
  }

  /**
   * Compares one and other module to bring them in alphabetical order
   *
   * @param {ModuleModel} a one module
   * @param {ModuleModel} b other module
   * @returns {number} negative, if one alphabetically infront of other; positive, if behind; 0 otherwise
   */
  private sortModuleModelsAlphabetically(
    a: ModuleModel,
    b: ModuleModel
  ): number {
    var nameA = a.name.toLowerCase();
    var nameB = b.name.toLowerCase();
    return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
  }

  /**
   * Converts name of object (module) null-safely to presentable string.
   *
   * @param {ModuleModel} model module to show name for
   * @returns {string} presentable string
   */
  public displayName(model: ModuleModel): string {
    return model && model.name ? model.name : '';
  }

  /**
   * Opens configuration dialog for module. Replaces returned & updated model in selectedModules.
   *
   * @param {ModuleModel} module module to open configuration dialog for
   */
  public openModuleConfigurationDialog(module: ModuleModel) {
    this.subscriptions.push(
      this.moduleConfigurationDialogComponent
        .open(ModuleConfigurationDialogComponent, {
          width: '80%',
          data: module,
        })
        .afterClosed()
        .subscribe((result) => {
          // dialog only has return when save clicked (and save successful)
          if (result) {
            const index = this.selectedModules.getValue().indexOf(module); // use module to retrieve index for new object

            if (index >= 0) {
              this.selectedModules.getValue().splice(index, 1, result); // replace with updated value from db
            }
          }
        })
    );
  }

  /**
   * Removes given module from selectedModules and add (back) to allModules.
   *
   * @param {ModuleModel} module module to delete from selection
   */
  public removeSelection(module: ModuleModel) {
    const index = this.selectedModules.getValue().indexOf(module);

    if (index >= 0) {
      this.selectedModules.getValue().splice(index, 1);
      this.allModules.push(module);
      this.moduleSelectionInput.updateValueAndValidity({
        emitEvent: true,
      }); // trigger (re-)evaluation current input as removed module might fit input string

      this.sendModuleSelectionChangedEvent();
    }
  }

  /**
   * Propagate autocomplete selection to selectedModules and remove from allModules accordingly.
   *
   * @param {MatAutocompleteSelectedEvent} event event containing selected option with module
   */
  public selected(event: MatAutocompleteSelectedEvent) {
    const module: ModuleModel = event?.option?.value;
    // both if's are just precaution and should always be true
    if (module) {
      this.selectedModules.getValue().push(module);

      const index = this.allModules.indexOf(module);
      if (index >= 0) {
        this.allModules.splice(index, 1);
        this.moduleSelectionInput.updateValueAndValidity({
          emitEvent: true,
        }); // trigger (re-)evaluation current input as removed module might fit input string
      }

      this.resetInput();
      this.sendModuleSelectionChangedEvent();
    }
  }

  /**
   * Resets values stored in input (current text) and input FormControl object
   */
  private resetInput() {
    if (this.moduleInput?.nativeElement?.value) {
      this.moduleInput.nativeElement.value = '';
    }
    this.moduleSelectionInput.setValue(null);
  }

  /**
   * Emits event with current modules stored in selectedModules.
   */
  private sendModuleSelectionChangedEvent() {
    this.moduleSelectionChanged.emit(this.selectedModules.getValue());
  }
}
