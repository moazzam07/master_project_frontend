import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { InfoErrorDialogComponent } from '../../components/info-error-dialog/info-error-dialog.component';
import { InfoErrorDialogConfiguration } from '../../models/models';

/**
 * Global error handler.
 * This class can be used in the pipe of an observable.
 * An HTTP error is intercepted and displayed in a dialog.
 */
@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerService {
  constructor(private dialog: MatDialog) {}

  /**
   * Intercepts HTTP error and displays it in a dialog.
   * @template T
   * @param {string} [operation='operation'] methode name or error message
   * @param {T} [result] generic HTTP error
   * @returns generic observable
   */
  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // error logging is only useful in dev mode
      // console.error(error);
      
      // Open error Dialog
      const dialogConfig: InfoErrorDialogConfiguration = {
        isError: true,
        displayText: `${operation} fehlgeschlagen`,
        buttonText: ['Schließen'],
        header: 'Informationen',
      };
      this.dialog.open(InfoErrorDialogComponent, {
        width: '50%',
        maxHeight: '75%',
        data: dialogConfig,
      });

      return of(result as T);
    };
  }
}
