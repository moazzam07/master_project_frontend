import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ErrorHandlerService } from '../error-handler/error-handler.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class MetricsService {
  private readonly API_ENDPOINT = 'http://localhost:8080';
  private readonly METRICS = '/metrics/';
  private readonly PROF_AMOUNT = 'professors/amount';
  private readonly PROF_NAMES = 'professors/names';
  private readonly SUBJECT_AMOUNT = 'subjects/amount';
  private readonly SUBJECT_NAMES = 'subjects/names';
  private readonly UNASSIGNED_SUBJECT_NAMES = 'subjects/unassignedNames';
  private readonly SUBJECT_UNASSIGNED = 'subjects/unassigned';

  public profAmountData$: BehaviorSubject<number> = new BehaviorSubject<number>(
    null
  );
  public profNamesData$: BehaviorSubject<string[]> = new BehaviorSubject<
    string[]
  >(null);
  public subjectAmountData$: BehaviorSubject<number> = new BehaviorSubject<
    number
  >(null);
  public subjectNamesData$: BehaviorSubject<string[]> = new BehaviorSubject<
    string[]
  >(null);
  public unassignedSubjectNamesData$: BehaviorSubject<
    string[]
  > = new BehaviorSubject<string[]>(null);
  public subjectUnassignedData$: BehaviorSubject<number> = new BehaviorSubject<
    number
  >(null);

  constructor(
    private http: HttpClient,
    private errorHandler:ErrorHandlerService
    ) {}

  public getProfAmount() {
    this.http
      .get(this.API_ENDPOINT + this.METRICS + this.PROF_AMOUNT)
      .pipe(catchError(this.errorHandler.handleError<any>("getProfAmount")))
      .subscribe((data: number) => {
        if (data) {
          this.profAmountData$.next(data);
        }
      });
  }
  public getProfNames() {
    this.http
      .get(this.API_ENDPOINT + this.METRICS + this.PROF_NAMES)
      .pipe(catchError(this.errorHandler.handleError<any>("getProfNames")))
      .subscribe((data: string[]) => {
        if (data) {
          this.profNamesData$.next(data);
        }
      });
  }
  public getSubjectAmount() {
    this.http
      .get(this.API_ENDPOINT + this.METRICS + this.SUBJECT_AMOUNT)
      .pipe(catchError(this.errorHandler.handleError<any>("getSubjectAmount")))
      .subscribe((data: number) => {
        if (data) {
          this.subjectAmountData$.next(data);
        }
      });
  }
  public getSubjectNames() {
    this.http
      .get(this.API_ENDPOINT + this.METRICS + this.SUBJECT_NAMES)
      .pipe(catchError(this.errorHandler.handleError<any>("getSubjectNames")))
      .subscribe((data: string[]) => {
        if (data) {
          this.subjectNamesData$.next(data);
        }
      });
  }
  public getUnassignedSubjectNames() {
    this.http
      .get(this.API_ENDPOINT + this.METRICS + this.UNASSIGNED_SUBJECT_NAMES)
      .pipe(catchError(this.errorHandler.handleError<any>("getUnassignedSubjectNames")))
      .subscribe((data: string[]) => {
        if (data) {
          this.unassignedSubjectNamesData$.next(data);
        }
      });
  }
  public getSubjectUnassigned() {
    this.http
      .get(this.API_ENDPOINT + this.METRICS + this.SUBJECT_UNASSIGNED)
      .pipe(catchError(this.errorHandler.handleError<any>("getSubjectUnassigned")))
      .subscribe((data: number) => {
        if (data === 0) {
          this.subjectUnassignedData$.next(999);
        } else {
          this.subjectUnassignedData$.next(data);
        }
      });
  }
}
