import { ErrorHandlerService } from './../error-handler/error-handler.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  BasicNamedModel,
  BasicReturnModel,
  ConstraintModel,
  SettingsModel,
  TimeSlotGroupModel,
} from './../../models/models';
import { catchError } from 'rxjs/operators';

/**
 * Documentation 
 *                                                      Methode
 * #Constraints
 * http://localhost:8080/constraint/get                 Get
 * http://localhost:8080/constraint/update              Post
 * http://localhost:8080/constraint/delete              Delete
 * 
 * #Modules
 * http://localhost:8080/module/save                    Post
 * http://localhost:8080/module/module/get              Get
 * http://localhost:8080/module/module/get/{id}         Get
 * http://localhost:8080/module/module/get/unassigned   Get
 * http://localhost:8080/module/module/get/sizes        Get
 * http://localhost:8080/module/module/delete/{id}      Delete
 * http://localhost:8080/module/module/update           Put
 * http://localhost:8080/module/module/delete           Delete
 * 
 * #PhaseOne
 * http://localhost:8080/execute/phase1                 Get
 * 
 * #PhaseTwo
 * http://localhost:8080/execute/phase2                 Post
 * 
 * #Professor
 * http://localhost:8080/professor/save                 Post
 * http://localhost:8080/module/professor/get           Get
 * http://localhost:8080/module/professor/get/{id}      Get
 * http://localhost:8080/module/professor/delete/{id}   Delete
 * http://localhost:8080/module/professor/delete        Delete
 * http://localhost:8080/module/professor/update        Put
 * 
 * #Settings
 * http://localhost:8080/settings/get                   Get
 * http://localhost:8080/settings/get/current           Get
 * http://localhost:8080/settings/update                Put
 * 
 * 
 */



/**
 * Service that communicates mainly with the backend 
 * Rest-call can be created generically 
 */
@Injectable({
  providedIn: 'root',
})
export class MainCommunicationService {
  //backend base url
  private readonly API_ENDPOINT = 'http://localhost:8080';
  private readonly GET = 'get';
  private readonly SAVE = 'save';
  private readonly UPDATE = 'update';
  private readonly DELETE = 'delete';
  private readonly EXECUTE = 'execute';

  public readonly PHASE1 = 'phase1';
  public readonly PHASE2 = 'phase2';
  public readonly PROFESSORS = 'professor';
  public readonly MODULE = 'module';
  public readonly UNASSIGNED = 'unassigned';
  public readonly SIZES = 'sizes';
  public readonly SETTINGS = 'settings';
  public readonly CURRENT = 'current';
  public readonly CONSTRAINT = 'constraint';

  constructor(
    private http: HttpClient,
    private errorHandler: ErrorHandlerService
  ) {}

  private get(url: string) {
    return this.http
      .get(url)
      .pipe(catchError(this.errorHandler.handleError<any>('get')));
  }

  private post(url: string, data: any) {
    return this.http
      .post(url, data)
      .pipe(catchError(this.errorHandler.handleError<any>('post')));
  }

  private put(url: string, data: any) {
    return this.http
      .put(url, data)
      .pipe(catchError(this.errorHandler.handleError<any>('put')));
  }

  private delete(url: string) {
    return this.http
      .delete(url)
      .pipe(catchError(this.errorHandler.handleError<any>('delete')));
  }

  private createUrl(...action: string[]): string {
    return [this.API_ENDPOINT, ...action].join('/');
  }

  private getBehaviourSubjectOfObservable(observable: Observable<Object>) {
    const behaviorSubject = new BehaviorSubject<any>(null);

    observable.subscribe((data) => {
      if (data) {
        behaviorSubject.next(data);
      }
    });

    return behaviorSubject;
  }

  public getAllData(urlParameter: string): BehaviorSubject<any[]> {
    const observable = this.get(this.createUrl(urlParameter, this.GET));
    return this.getBehaviourSubjectOfObservable(observable);
  }

  public getSpecificData(
    urlParameterBase: string,
    urlParameterSpecific: string
  ): BehaviorSubject<any> {
    const observable = this.get(
      this.createUrl(urlParameterBase, this.GET, urlParameterSpecific)
    );
    return this.getBehaviourSubjectOfObservable(observable);
  }

  public createData(
    urlParameter: string,
    data: any
  ): BehaviorSubject<BasicReturnModel> {
    const observable = this.post(this.createUrl(urlParameter, this.SAVE), data);
    return this.getBehaviourSubjectOfObservable(observable);
  }

  public postConstraintData(
    urlParameter: string,
    data: ConstraintModel
  ): BehaviorSubject<any> {
    const observable = this.post(this.createUrl(urlParameter, this.SAVE), data);
    return this.getBehaviourSubjectOfObservable(observable);
  }

  public updateData(
    urlParameter: string,
    data: any
  ): BehaviorSubject<BasicReturnModel> {
    const observable = this.put(
      this.createUrl(urlParameter, this.UPDATE),
      data
    );
    return this.getBehaviourSubjectOfObservable(observable);
  }

  public deleteData(
    urlParameter: string,
    data: BasicNamedModel
  ): BehaviorSubject<BasicReturnModel> {
    const observable = this.delete(
      this.createUrl(urlParameter, this.DELETE, String(data.id))
    );
    return this.getBehaviourSubjectOfObservable(observable);
  }

  public executeTask(
    urlParameter: string,
    data?: TimeSlotGroupModel[]
  ): BehaviorSubject<BasicReturnModel | any> {
    var observable;
    if (data) {
      observable = this.post(this.createUrl(this.EXECUTE, urlParameter), data);
    } else {
      observable = this.get(this.createUrl(this.EXECUTE, urlParameter));
    }

    return this.getBehaviourSubjectOfObservable(observable);
  }
}
