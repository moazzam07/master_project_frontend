import { Injectable } from '@angular/core';
import { TimeSlotGroupModel } from '../../models/models';

@Injectable()
export class PhaseOneTwoService {
  private _selectedPhaseOneSolution: TimeSlotGroupModel[] = [];

  public set selectedPhaseOneSolution(
    selectedPhaseOneSolution: TimeSlotGroupModel[]
  ) {
    this._selectedPhaseOneSolution = selectedPhaseOneSolution;
  }

  public get selectedPhaseOneSolution(): TimeSlotGroupModel[] {
    return this._selectedPhaseOneSolution;
  }
}
