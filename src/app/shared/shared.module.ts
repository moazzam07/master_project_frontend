import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreditDialogComponent } from './components/credit-dialog/credit-dialog.component';
import { CsvImportDialogComponent } from './components/csv-import-dialog/csv-import-dialog.component';
import { EditNameDialogComponent } from './components/edit-name-dialog/edit-name-dialog.component';
import { HtmlToPdfDialogComponent } from './components/html-to-pdf-dialog/html-to-pdf-dialog.component';
import { InfoErrorDialogComponent } from './components/info-error-dialog/info-error-dialog.component';
import { InputFormFieldComponent } from './components/input-form-field/input-form-field.component';
import { LegendWeekCalenderComponent } from './components/legend-week-calender/legend-week-calender.component';
import { ModuleConfigurationDialogComponent } from './components/module-configuration-dialog/module-configuration-dialog.component';
import { ModuleConfigurationComponent } from './components/module-configuration/module-configuration.component';
import { ModuleSelectionComponent } from './components/module-selection/module-selection.component';
import { SelectOptionFormFieldComponent } from './components/select-option-form-field/select-option-form-field.component';
import { TypeaheadComponent } from './components/typeahead/typeahead.component';
import { WeekCalenderWeightingDialogComponent } from './components/week-calender-weighting-dialog/week-calender-weighting-dialog.component';
import { WeekCalenderComponent } from './components/week-calender/week-calender.component';
import { MainCommunicationService } from './services/load-data/main-communication.service';
import { PhaseOneTwoService } from './services/injection/phase-one-two-injectable.service';
import { DemoMaterialModule } from './utils/material-module';

@NgModule({
  declarations: [
    CreditDialogComponent,
    CsvImportDialogComponent,
    EditNameDialogComponent,
    InputFormFieldComponent,
    ModuleConfigurationComponent,
    ModuleConfigurationDialogComponent,
    ModuleSelectionComponent,
    SelectOptionFormFieldComponent,
    TypeaheadComponent,
    WeekCalenderComponent,
    WeekCalenderWeightingDialogComponent,
    CsvImportDialogComponent,
    InfoErrorDialogComponent,
    HtmlToPdfDialogComponent,
    LegendWeekCalenderComponent,
  ],
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  exports: [
    CreditDialogComponent,
    EditNameDialogComponent,
    InputFormFieldComponent,
    ModuleConfigurationComponent,
    ModuleConfigurationDialogComponent,
    ModuleSelectionComponent,
    SelectOptionFormFieldComponent,
    TypeaheadComponent,
    WeekCalenderComponent,
    LegendWeekCalenderComponent,
  ],
  entryComponents: [
    CreditDialogComponent,
    CsvImportDialogComponent,
    EditNameDialogComponent,
    InfoErrorDialogComponent,
    WeekCalenderWeightingDialogComponent,
    InfoErrorDialogComponent,
    HtmlToPdfDialogComponent,
  ],
  providers: [MainCommunicationService, PhaseOneTwoService],
})
export class SharedModule {}
