// ************************************************************************************************
// models for communication service
// ************************************************************************************************
export interface BasicNamedModel {
  id: number;
  name: string;
}
export interface BasicReturnModel {
  state: number;
  data: string;
  message: string;
}
export interface BasicTimeSlotModel {
  day: number;
  time: number;
}
export interface WeightedTimeSlotModel extends BasicTimeSlotModel {
  weighting?: number;
}
export interface ProfessorModel
  extends BasicNamedModel,
    ProfessorAvailabilityModel {
  modules: ModuleModel[];
  adjacencyCostMultiplier: number;
}
export interface ProfessorAvailabilityModel {
  availableTimeslots: BasicTimeSlotModel[];
  possibleTimeslots: WeightedTimeSlotModel[];
  unavailableTimeslots: BasicTimeSlotModel[];
}
export interface ModuleModel extends BasicNamedModel {
  shortName: string;
  theoryPraxisType: 'na' | 'theory' | 'praxis';
  numberOfLectures: number;
  numberOfExercises: number;
  numberOfStudents: number;
}
export interface ConstraintModel {
  id: number;
  weighting: number;
  modules: ModuleModel[];
}
export interface SettingsModel {
  id: number;

  // phase 1
  numberOfSolutions: number;
  numberOfSlotsPerDay: number;
  numberOfEarlySlots: number;
  numberOfLunchSlots: number;
  numberOfLateSlots: number;
  relativeSolutionGap: number; // (double)
  timeslotGroupConfigurations: PhaseOneTimeslotGroupConfiguration[];

  // phase 2
  theoryPraxisSeparationWeight: number;
  professorCostMultiplierPossibleSSA: number;
  professorCostMultiplierUnavailableSSA: number;
  adjacencyTaxMBT: number;
  timeLimitInMinutesMBT: number; // (double)
  constraintTax: number; // (double)
  possibleWeightingTax: number; // (double)
  phaseTwoSelection: 'SSA' | 'ILP' | 'MBT';
}
// phase 1 return models
export interface PhaseOneTimeslotGroupConfiguration {
  size: number;
  amount: number;
  presentModules: number;
}
export interface PhaseOneReturnModel {
  solutionStatus: boolean;
  solutionMessage: string;
  solutionList: PhaseOneSolutionModel[];
}
export interface PhaseOneSolutionModel {
  id: number; // not filled by phase 1..
  timeSlotGroups: TimeSlotGroupModel[];
  objectiveFunctionValue: number;
}
export interface TimeSlotGroupModel {
  id: number;
  timeSlots: BasicTimeSlotModel[];
  objectiveValue: number;
}
// phase 2 return models
export interface PhaseTwoModel {
  returnCode: number;
  numberOfUnallocatedLectures: number;
  completedSuccessfully: boolean;
  isFeasible: boolean;
  hasUnallocatedLectures: boolean;
  groupedModules: PhaseTwoTimeslotGroupModuleModel[];
  unallocatedModules: ModuleModel[];
}
export interface PhaseTwoTimeslotGroupModuleModel {
  timeslotGroup: TimeSlotGroupModel;
  modules: ModuleModel[];
}

// ************************************************************************************************
// models for week-calender component
// ************************************************************************************************
export interface WeekCalenderConfigurationModel {
  amount: number;
  lables: string[];
}
export interface BasicTileModel {
  id: number;
  isBlocked: boolean;
  timeSlot: BasicTimeSlotModel;
}
export interface ProfessorConfigurationTileModel extends BasicTileModel {
  state: 'available' | 'possible' | 'unavailable';
  isCurrentSelection: boolean;
}
export interface PhaseOneTwoTileModel extends BasicTileModel {
  timeSlotGroup: TimeSlotGroupModel;
  color: string;
}
export interface PhaseOneTileModel extends PhaseOneTwoTileModel {}
export interface PhaseTwoTileModel extends BasicTileModel {
  modules: ModuleModel[];
}
export interface ProfessorConfigurationWeekCalenderTransitionModel
  extends ProfessorAvailabilityModel {}

// ************************************************************************************************
// other component models
// ************************************************************************************************
export interface SelectOptionModel {
  value: string;
  description: string;
}
export interface EditNameDialogModel {
  urlDirectoryPath: string;
  model: BasicNamedModel;
}
export interface InfoErrorDialogConfiguration {
  isError: boolean;
  displayText: string;
  buttonText: string[]; // button pressed action handled by opening component (subscribe onClose weill return single clicked buttonText)
  header: string;
}

export interface PDFExportConfigurations {
  tableData: HTMLElement;
  legendeData: HTMLElement;
  showLegend: boolean;
  pdfName: string;
  moduls: ModuleModel;
}
