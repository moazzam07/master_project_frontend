import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import { ModuleConfigurationDialogComponent } from '../shared/components/module-configuration-dialog/module-configuration-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { ConstraintConfigurationPageComponent } from './constraint-configuration-page/constraint-configuration-page.component';
import { ConstraintConfigurationRoutingModule } from './constraint-configuration-routing.module';

@NgModule({
  declarations: [ConstraintConfigurationPageComponent],
  imports: [
    CommonModule,
    ConstraintConfigurationRoutingModule,
    SharedModule,
    MatExpansionModule,
    MatSliderModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [MatSliderModule, MatExpansionModule],
  entryComponents: [ModuleConfigurationDialogComponent],
})
export class ConstraintConfigurationModule {}
