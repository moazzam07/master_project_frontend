import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConstraintConfigurationPageComponent } from './constraint-configuration-page/constraint-configuration-page.component';

const routes: Routes = [
  { path: '', component: ConstraintConfigurationPageComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConstraintConfigurationRoutingModule {}
