import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { InfoErrorDialogComponent } from 'src/app/shared/components/info-error-dialog/info-error-dialog.component';
import { InfoErrorDialogConfiguration, ModuleModel } from '../../shared/models/models';
import { MainCommunicationService } from '../../shared/services/load-data/main-communication.service';

/**
 * @author muhammad.moazzam
 */
@Component({
  selector: 'constraint-configuration-page',
  templateUrl: './constraint-configuration-page.component.html',
  styleUrls: ['./constraint-configuration-page.component.scss'],
  providers: [MainCommunicationService],
})
export class ConstraintConfigurationPageComponent implements OnInit, OnDestroy {
  constraintForm: FormGroup;
  value = false;
  test = [];
  preselectedModules = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    public mainCommunicationService: MainCommunicationService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.createForm();
    this.subscriptions.push(
      this.mainCommunicationService
        .getAllData(this.mainCommunicationService.CONSTRAINT)
        .subscribe((data) => {
          if (data) {
            this.fetchConstraints(data);
          }
        })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
  

  createForm() {
    this.constraintForm = this.fb.group({
      id: 1,
      constraint: this.fb.array([this.createConstraintArray()]),
    });
  }

  createConstraintArray() {
    return this.fb.group({
      weighting: ['', Validators.required],
      modules: this.fb.array([]),
    });
  }

  getConstraint(form) {
    return form.controls.constraint.controls;
  }

  addConstraint(isInit?: boolean) {
    const control = <FormArray>this.constraintForm.get('constraint');
    control.push(this.createConstraintArray());
    this.preselectedModules.push([]);
    if (!isInit) {
      this.openSnackBar('Bedingungen hinzugefügt!', 'X');
    }
  }

  removeConstraint(i) {
    const control = <FormArray>this.constraintForm.get('constraint');

    control.removeAt(i);
    this.preselectedModules.splice(i, 1);

    this.openSnackBar('Bedingungen entfernt!', 'X');
  }

  onSubmitClick() {
    const Data = this.constraintForm.get('constraint').value;

    this.subscriptions.push(
      this.mainCommunicationService
        .updateData(this.mainCommunicationService.CONSTRAINT, Data)
        .subscribe((response) => {
          if (response) {
          }
        })
    );

    this.openSnackBar('Bedingungen gespeichert!', 'X');
  }
  public moduleSelectionChanged($event: ModuleModel[], personFormGroupIndex) {
    if ($event) {
      if ($event.length < 3) {
        const personFormList = this.constraintForm.get(
          'constraint'
        ) as FormArray;
        const personFormGroup = personFormList.controls[
          personFormGroupIndex
        ] as FormGroup;
        const modulesFormList = personFormGroup.get('modules') as FormArray;
        const selectedModules = $event;
        modulesFormList.clear();
        selectedModules.forEach((item) => {
          modulesFormList.push(this.fb.group(item));
        });
      }
    }
  }

  fetchConstraints(constraintData) {
    this.constraintForm.patchValue(constraintData);
    const constraintList = this.constraintForm.controls.constraint as FormArray;

    constraintList.removeAt(0);

    constraintData.forEach((item, index) => {
      const constraintFormGroup = this.fb.group({
        weighting: item.weighting,
        modules: this.fb.array(
          item.modules.map((moduleItem) => {
            return this.fb.group(moduleItem);
          })
        ),
      });

      this.preselectedModules[index] = item.modules;

      constraintList.push(constraintFormGroup);
    });
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }

  openInfoDialog() {
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText:
        'Hier lassen  sich  Nebenbedingungen  für  den  Algorithmus  formulieren,  die  darauf  abzielen, bestimmte Paare von Modulen frei von ̈Überschneidung zu halten. Relevanz für die Praxis erhält dies dadurch, dass es aus studentischer Sicht oftmals Kombinationen von Modulen gibt, die gerne gleichzeitig belegt werden. In der Bildschirmmaske lassen sich zwei Module erfassten. Per Schieberegler  lassen  sich  eine  Gewichtung  der  Trennung  der  gewählten  Module  einstellen  –  je  höher der Wert, desto stärker wird die Zuweisung in verschiedene Slots berücksichtigt.',
      buttonText: ['Schließen'],
      header: 'Informationen',
    };
    this.dialog.open(InfoErrorDialogComponent, {
      width: '50%',
      height: '25%',
      data: dialogConfig,
    });
  }
}
