import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConstraintConfigurationPageComponent } from './constraint-configuration-page.component';

describe('SoftConstraintPageComponent', () => {
  let component: ConstraintConfigurationPageComponent;
  let fixture: ComponentFixture<ConstraintConfigurationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConstraintConfigurationPageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstraintConfigurationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
