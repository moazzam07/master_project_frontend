import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsConfigurationPageComponent } from './settings-configuration-page/settings-configuration-page.component';

const routes: Routes = [
  { path: '', component: SettingsConfigurationPageComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsConfigurationRoutingModule {}
