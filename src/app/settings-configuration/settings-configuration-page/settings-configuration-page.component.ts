import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import * as configData from 'config.json';
import { Subscription } from 'rxjs';
import { InfoErrorDialogComponent } from 'src/app/shared/components/info-error-dialog/info-error-dialog.component';
import { PhaseOneTwoService } from 'src/app/shared/services/injection/phase-one-two-injectable.service';
import { MainCommunicationService } from 'src/app/shared/services/load-data/main-communication.service';
import {
  InfoErrorDialogConfiguration,
  SettingsModel,
} from '../../shared/models/models';
import {
  PhaseOneTimeslotGroupConfiguration,
  SelectOptionModel,
} from './../../shared/models/models';

@Component({
  selector: 'settings-configuration-page',
  templateUrl: './settings-configuration-page.component.html',
  styleUrls: ['./settings-configuration-page.component.scss'],
  providers: [MainCommunicationService],
})
export class SettingsConfigurationPageComponent implements OnInit, OnDestroy {
  public readonly NUMBER_OF_AVAILABLE_SLOTS_TOTAL = 23; // total number of available slots to be assigned (all slots - blocked slots)
  private readonly NO_EVENT = { emitEvent: false };

  /**
   * defaulting for controls done here for better visibility & structure
   */
  private currentSettings: SettingsModel = {
    // phase 1
    numberOfSolutions: 10,
    numberOfSlotsPerDay: 2,
    numberOfEarlySlots: 1,
    numberOfLunchSlots: 1,
    numberOfLateSlots: 1,
    relativeSolutionGap: 0.0,
    timeslotGroupConfigurations: [],

    // phase 2
    theoryPraxisSeparationWeight: 0,
    constraintTax: 1.0,
    possibleWeightingTax: 1.0,
    phaseTwoSelection: 'SSA',
    professorCostMultiplierPossibleSSA: 5,
    professorCostMultiplierUnavailableSSA: 10,
    adjacencyTaxMBT: 1,
    timeLimitInMinutesMBT: 1.0,
  } as SettingsModel;

  public settingsConfigurationGroup: FormGroup;
  // phase 1
  public numberOfSolutionsControl = new FormControl(
    this.currentSettings.numberOfSolutions,
    [Validators.min(0),Validators.pattern(/^\d+$/)]
  );
  public numberOfSlotsPerDayControl = new FormControl(
    this.currentSettings.numberOfSlotsPerDay,
    [Validators.min(0), Validators.max(5),Validators.pattern(/^\d+$/)]
  );
  public numberOfEarlySlotsControl = new FormControl(
    this.currentSettings.numberOfEarlySlots,
    [Validators.min(0), Validators.max(5),Validators.pattern(/^\d+$/)]
  );
  public numberOfLunchSlotsControl = new FormControl(
    this.currentSettings.numberOfLunchSlots,
    [Validators.min(0), Validators.max(5),Validators.pattern(/^\d+$/)]
  );
  public numberOfLateSlotsControl = new FormControl(
    this.currentSettings.numberOfLateSlots,
    [Validators.min(0), Validators.max(5),Validators.pattern(/^\d+$/)]
  );
  public relativeSolutionGapControl = new FormControl(
    this.currentSettings.relativeSolutionGap,
    Validators.min(0)
  );
  public timeslotGroupConfigurationControlMap: Map<
    FormControl,
    PhaseOneTimeslotGroupConfiguration
  > = new Map();
  public numberOfRemainingAvailableSlots: number = this
    .NUMBER_OF_AVAILABLE_SLOTS_TOTAL;

  // phase 2
  public theoryPraxisSeparationWeightControl = new FormControl(
    this.currentSettings.theoryPraxisSeparationWeight,
    [Validators.min(0), Validators.max(100),Validators.pattern(/^\d+$/)]
  );
  public constraintTaxControl = new FormControl(
    this.currentSettings.constraintTax,
    Validators.min(0)
  );
  public possibleWeightingTaxControl = new FormControl(
    this.currentSettings.possibleWeightingTax,
    Validators.min(0)
  );
  public phaseTwoSelectionControl = new FormControl(
    this.currentSettings.phaseTwoSelection
  );
  public phaseTwoSelectionOptions: SelectOptionModel[] =
    configData.phaseTwoSelectionOptions;
  // [
  //   { value: 'SSA', description: 'Sort, Sort, Allocate (SSA)' },
  //   { value: 'ILP', description: 'Integer Linear Programming (ILP)' },
  //   { value: 'MBT', description: 'Mixed Basic Techniques (MBT)' },
  // ];

  // SSA
  public professorCostMultiplierPossibleSSAControl = new FormControl(
    this.currentSettings.professorCostMultiplierPossibleSSA,
    [Validators.min(0),Validators.pattern(/^\d+$/)]
  );
  public professorCostMultiplierUnavailableSSAControl = new FormControl(
    this.currentSettings.professorCostMultiplierUnavailableSSA,
    [Validators.min(0),,Validators.pattern(/^\d+$/)]
  );
  // MBT
  public adjacencyTaxMBTControl = new FormControl(
    this.currentSettings.adjacencyTaxMBT,
    [Validators.min(0),,Validators.pattern(/^\d+$/)]
  );
  public timeLimitInMinutesMBTControl = new FormControl(
    this.currentSettings.timeLimitInMinutesMBT,
    Validators.min(0.01)
  );

  private subscriptions: Subscription[] = [];

  constructor(
    public phaseOneTwoService: PhaseOneTwoService,
    private mainCommunicationService: MainCommunicationService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router
  ) {
    // define FormGroup
    this.settingsConfigurationGroup = this.formBuilder.group({
      // phase 1
      numberOfSolutions: this.numberOfSolutionsControl,
      numberOfSlotsPerDay: this.numberOfSlotsPerDayControl,
      numberOfEarlySlots: this.numberOfEarlySlotsControl,
      numberOfLunchSlots: this.numberOfLunchSlotsControl,
      numberOfLateSlots: this.numberOfLateSlotsControl,
      relativeSolutionGap: this.relativeSolutionGapControl,
      // this.timeslotGroupConfigurationControls are added to group individually (due to dynamic control creation/definition)

      // phase 2
      theoryPraxisSeparationWeight: this.theoryPraxisSeparationWeightControl,
      constraintTax: this.constraintTaxControl,
      possibleWeightingTax: this.possibleWeightingTaxControl,
      phaseTwoSelection: this.phaseTwoSelectionControl,
      professorCostMultiplierPossibleSSA: this
        .professorCostMultiplierPossibleSSAControl,
      professorCostMultiplierUnavailableSSA: this
        .professorCostMultiplierUnavailableSSAControl,
      adjacencyTaxMBT: this.adjacencyTaxMBTControl,
      timeLimitInMinutesMBT: this.timeLimitInMinutesMBTControl,
    });
  }

  ngOnInit() {
    this.loadSettings();
    this.addChangeListener();
  }

  /**
   * Loads settings from db. Creates intially if none present.
   */
  private loadSettings() {
    this.subscriptions.push(
      this.mainCommunicationService
        .getSpecificData(
          this.mainCommunicationService.SETTINGS,
          this.mainCommunicationService.CURRENT
        )
        .subscribe((response: SettingsModel) => {
          if (response) {
            this.currentSettings = response;

            // phase 1
            this.numberOfSolutionsControl.setValue(
              response.numberOfSolutions,
              this.NO_EVENT
            );
            this.numberOfSlotsPerDayControl.setValue(
              response.numberOfSlotsPerDay,
              this.NO_EVENT
            );
            this.numberOfEarlySlotsControl.setValue(
              response.numberOfEarlySlots,
              this.NO_EVENT
            );
            this.numberOfLunchSlotsControl.setValue(
              response.numberOfLunchSlots,
              this.NO_EVENT
            );
            this.numberOfLateSlotsControl.setValue(
              response.numberOfLateSlots,
              this.NO_EVENT
            );
            this.relativeSolutionGapControl.setValue(
              response.relativeSolutionGap,
              this.NO_EVENT
            );
            // After response timeslotGroupConfigurationControls can be created dynamically
            // (as defaulting is now possible - otherwise default settings might be updated and the retrieved configuration values therefore lost)
            this.createTimeslotGroupConfigurationControls();

            // phase 2
            this.theoryPraxisSeparationWeightControl.setValue(
              response.theoryPraxisSeparationWeight,
              this.NO_EVENT
            );
            this.constraintTaxControl.setValue(
              response.constraintTax,
              this.NO_EVENT
            );
            this.possibleWeightingTaxControl.setValue(
              response.possibleWeightingTax,
              this.NO_EVENT
            );
            this.phaseTwoSelectionControl.setValue(
              response.phaseTwoSelection,
              this.NO_EVENT
            );
            this.professorCostMultiplierPossibleSSAControl.setValue(
              response.professorCostMultiplierPossibleSSA,
              this.NO_EVENT
            );
            this.professorCostMultiplierUnavailableSSAControl.setValue(
              response.professorCostMultiplierUnavailableSSA,
              this.NO_EVENT
            );
            this.adjacencyTaxMBTControl.setValue(
              response.adjacencyTaxMBT,
              this.NO_EVENT
            );
            this.timeLimitInMinutesMBTControl.setValue(
              response.timeLimitInMinutesMBT,
              this.NO_EVENT
            );
          }
        })
    );
  }

  /**
   * Adds change listener for FormGroup of settings.
   */
  private addChangeListener() {
    this.subscriptions.push(
      this.settingsConfigurationGroup.valueChanges.subscribe(
        (data: SettingsModel) => {
          // only stored internally when FormGroup is valid
          if (data && this.settingsConfigurationGroup.valid) {
            // phase 1
            this.currentSettings.numberOfSolutions = data.numberOfSolutions;
            this.currentSettings.numberOfSlotsPerDay = data.numberOfSlotsPerDay;
            this.currentSettings.numberOfEarlySlots = data.numberOfEarlySlots;
            this.currentSettings.numberOfLunchSlots = data.numberOfLunchSlots;
            this.currentSettings.numberOfLateSlots = data.numberOfLateSlots;
            this.currentSettings.relativeSolutionGap = data.relativeSolutionGap;
            // this.currentSettings.timeslotGroupConfigurations has own valueChanges subscription (due to dynamic control creation/definition, see: #createTimeslotGroupConfigurationControls())

            // phase 2
            this.currentSettings.theoryPraxisSeparationWeight =
              data.theoryPraxisSeparationWeight;
            this.currentSettings.constraintTax = data.constraintTax;
            this.currentSettings.possibleWeightingTax =
              data.possibleWeightingTax;
            this.currentSettings.phaseTwoSelection = data.phaseTwoSelection;
            this.currentSettings.professorCostMultiplierPossibleSSA =
              data.professorCostMultiplierPossibleSSA;
            this.currentSettings.professorCostMultiplierUnavailableSSA =
              data.professorCostMultiplierUnavailableSSA;
            this.currentSettings.adjacencyTaxMBT = data.adjacencyTaxMBT;
            this.currentSettings.timeLimitInMinutesMBT =
              data.timeLimitInMinutesMBT;
          }
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Creates FormControl with fitting validators dynamically depending on different configurations for each "unique" module sizes found in db.
   */
  private createTimeslotGroupConfigurationControls() {
    this.subscriptions.push(
      this.mainCommunicationService
        .getSpecificData(
          this.mainCommunicationService.MODULE,
          this.mainCommunicationService.SIZES
        )
        .subscribe((response: PhaseOneTimeslotGroupConfiguration[]) => {
          if (response) {
            // store old settings and reset them internally to not keep irrelevant values in db (sizes for that no module exist anymore)
            const oldTimeslotGroupConfigurations = this.currentSettings
              .timeslotGroupConfigurations;
            this.currentSettings.timeslotGroupConfigurations = [];

            response
              .sort((a, b) => a.size - b.size)
              .forEach((configuration) => {
                this.createControl(
                  oldTimeslotGroupConfigurations,
                  configuration
                );
              });

            this.determineNewValidatorsForTimeslotGroupConfigurationControls();
          }
        })
    );
  }

  /**
   * Creates single FormControl for configuration.
   * Uses oldTimeslotGroupConfigurations as default value if existed previously.
   *
   * @param {PhaseOneTimeslotGroupConfiguration[]} oldTimeslotGroupConfigurations configurations previously set in db
   * @param {PhaseOneTimeslotGroupConfiguration} configuration (new) configuration to add control for
   */
  private createControl(
    oldTimeslotGroupConfigurations: PhaseOneTimeslotGroupConfiguration[],
    configuration: PhaseOneTimeslotGroupConfiguration
  ) {
    // check if for configuration (via module size) if there was a value set previously
    const availableConfiguration = oldTimeslotGroupConfigurations.find(
      (c) => c.size == configuration.size
    );
    if (availableConfiguration) {
      // if present still update number of present modules of previous configuration
      // (since possibly outdated value in db as no update is done automatically)
      availableConfiguration.presentModules = configuration.presentModules;
    }

    const internalConfiguration = availableConfiguration
      ? availableConfiguration
      : configuration;
    // (re-)add configuration to internal settings
    this.currentSettings.timeslotGroupConfigurations.push(
      internalConfiguration
    );

    const timeslotGroupConfigurationControl = new FormControl(
      internalConfiguration.amount
    );
    // add new control to map to iterate over it (html + logic) with easily keeping connection between control and configuration
    this.timeslotGroupConfigurationControlMap.set(
      timeslotGroupConfigurationControl,
      internalConfiguration
    );

    // add to group to benefit from GroupValidator (e. g. for save button disabling)
    this.settingsConfigurationGroup.addControl(
      'timeslotGroupConfigurationControl' + internalConfiguration.size,
      timeslotGroupConfigurationControl
    );

    this.registerValueChangesSubscription(
      timeslotGroupConfigurationControl,
      internalConfiguration
    );
  }

  /**
   * Registers individual valueChanges-listener for updating according value in internally stored settings.
   * Updates validators according to value change.
   *
   * @param {FormControl} timeslotGroupConfigurationControl control to set listener for
   * @param {PhaseOneTimeslotGroupConfiguration} internalConfiguration configuration to change value on
   */
  private registerValueChangesSubscription(
    timeslotGroupConfigurationControl: FormControl,
    internalConfiguration: PhaseOneTimeslotGroupConfiguration
  ) {
    timeslotGroupConfigurationControl.valueChanges.subscribe((value) => {
      internalConfiguration.amount = value;
      this.determineNewValidatorsForTimeslotGroupConfigurationControls();
    });
  }

  /**
   * Determines new validators (min & max value) for all controls of timeslotGroupConfigurationControlMap
   * as with each change it is necessary to update the relevant Validators so in total not more than possible slots can be selected.
   */
  private determineNewValidatorsForTimeslotGroupConfigurationControls() {
    this.calculateAvailableSlots();

    this.timeslotGroupConfigurationControlMap.forEach(
      (configuration, control) => {
        control.setValidators([
          Validators.min(0),
          Validators.max(
            this.calculateMaximalPossibleValue(
              control.value,
              configuration.size
            )
          ),
        ]);
      }
    );
  }

  /**
   * Determines the number of remaining avaiable slots by and stores it internally in numberOfRemainingAvailableSlots:
   *
   * Subtracting the sum of each (currently chosen value times its group size) from the total amount available slots.
   */
  private calculateAvailableSlots() {
    var numberOfRemainingAvailableSlots = this.NUMBER_OF_AVAILABLE_SLOTS_TOTAL;
    this.timeslotGroupConfigurationControlMap.forEach(
      (configuration, control) =>
        (numberOfRemainingAvailableSlots -= control.value * configuration.size)
    );
    this.numberOfRemainingAvailableSlots = numberOfRemainingAvailableSlots;
  }

  /**
   * Determines the maximal value that should be selectable for currently chosen value and group size by:
   *
   *
   * Adding the rounded maximal additionally possible number to the currently chosen value.
   * -> If maximal additionally possible number is negative it will only allow a lower value than currently accordingly.
   * -> If sum is negative in total it will return 0 as otherwise the validation would be infeasible.
   *
   *
   * @param {number} currentControlValue current value of control
   * @param {number} configurationSize module size in configuration
   * @returns {number}
   */
  public calculateMaximalPossibleValue(
    currentControlValue: number,
    configurationSize: number
  ): number {
    return Math.max(
      0,
      +currentControlValue + // FormControl value is any and it is passed as string (eventhough input is number), by adding the string itself we make sure that it not results in string concatination
        Math.floor(this.numberOfRemainingAvailableSlots / configurationSize)
    );
  }

  /**
   * Saves the internally stored settings to the db.
   *
   * On successful update: Navigates to phase-two page.
   */
  public goToPhaseTwo() {
    this.saveSettingInternally(true);
  }

  /**
   * Saves the internally stored settings to the db.
   *
   * On successful update:
   * - Notifies user accordingly.
   */
  public saveSettings() {
    this.saveSettingInternally(false);
  }

  /**
   * Saves the internally stored settings to the db.
   *
   * On successful update:
   * - Notifies user accordingly.
   * - If goToPhase2 true, navigate to phase 2
   *
   * @param {boolean} goToPhase2 if true, navigates to phase 2
   */
  private saveSettingInternally(goToPhase2: boolean) {
    this.subscriptions.push(
      this.mainCommunicationService
        .updateData(
          this.mainCommunicationService.SETTINGS,
          this.currentSettings
        )
        .subscribe((data) => {
          if (data) {
            this.openSnackBar('Einstellungen gespeichert!', 'X');

            if (goToPhase2) {
              this.router.navigate(['phase-two']);
            }
          }
        })
    );
  }

  public openGroupSizeInfoDialog() {
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText:
        'Größe der Slotgruppen bietet die Möglichkeit festzulegen, aus wieviele Slots die einzelnen Slotgruppen bestehen sollen und wieviele es von jeder Slotgruppe geben soll. ' +
        'Auf Basis der angegebenen Menge der jeweiligen Slotgruppengröße werden in Phase 1 die entsprechend Lösungen bestimmt. ' +
        'Diese Anzahl darf in Summe nicht größer als die Gesamtzahl der verfügbaren Slots sein (insgesamt: ' +
        this.NUMBER_OF_AVAILABLE_SLOTS_TOTAL +
        ' Slots).',
      buttonText: ['Schließen'],
      header: 'Informationen',
    };
    this.dialog.open(InfoErrorDialogComponent, {
      width: '50%',
      maxHeight: '75%',
      data: dialogConfig,
    });
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }
}
