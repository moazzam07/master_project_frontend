import { async, ComponentFixture, TestBed } from '@angular/core/testing';

describe('SettingsConfigurationPageComponent', () => {
  let component: SettingsConfigurationPageComponent;
  let fixture: ComponentFixture<SettingsConfigurationPageComponent>;

  // Implement tests

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsConfigurationPageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigurationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
