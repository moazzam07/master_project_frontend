import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
import { SettingsConfigurationPageComponent } from './settings-configuration-page/settings-configuration-page.component';
import { SettingsConfigurationRoutingModule } from './settings-configuration-routing.module';

@NgModule({
  declarations: [SettingsConfigurationPageComponent],
  imports: [
    CommonModule,
    SettingsConfigurationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
  ],
})
export class SettingsConfigurationModule {}
