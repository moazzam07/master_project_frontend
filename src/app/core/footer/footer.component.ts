import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreditDialogComponent } from 'src/app/shared/components/credit-dialog/credit-dialog.component';

/**
 * This class represents the footer of the application
 */
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  constructor(private dialog: MatDialog) {}

  /**
   * Opens a dialog in which the credits are displayed.
   */
  openCreditDialog() {
    const dialogRef = this.dialog.open(CreditDialogComponent, {
      width: '700px',
    });
  }
}
