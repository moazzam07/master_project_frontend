import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ModuleConfigurationDialogComponent } from '../shared/components/module-configuration-dialog/module-configuration-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { PhaseTwoPageComponent } from './phase-two-page/phase-two-page.component';
import { PhaseTwoRoutingModule } from './phase-two-routing.module';

@NgModule({
  declarations: [PhaseTwoPageComponent],
  imports: [
    CommonModule,
    PhaseTwoRoutingModule,
    SharedModule,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
  entryComponents: [ModuleConfigurationDialogComponent],
})
export class PhaseTwoModule {}
