import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PhaseTwoPageComponent } from './phase-two-page.component';

describe('PhaseTwoPageComponent', () => {
  let component: PhaseTwoPageComponent;
  let fixture: ComponentFixture<PhaseTwoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhaseTwoPageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseTwoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
