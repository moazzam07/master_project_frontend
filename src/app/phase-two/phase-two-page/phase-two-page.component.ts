import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ModuleModel, PhaseTwoModel, PhaseTwoTimeslotGroupModuleModel } from 'src/app/shared/models/models';
import { MainCommunicationService } from 'src/app/shared/services/load-data/main-communication.service';
import { PhaseOneTwoService } from 'src/app/shared/services/injection/phase-one-two-injectable.service';
import { HtmlToPdfDialogComponent } from './../../shared/components/html-to-pdf-dialog/html-to-pdf-dialog.component';
import {
  PDFExportConfigurations,
  ProfessorModel,
} from './../../shared/models/models';

@Component({
  selector: 'phase-two-page',
  templateUrl: './phase-two-page.component.html',
  styleUrls: ['./phase-two-page.component.scss'],
})
export class PhaseTwoPageComponent implements OnInit, OnDestroy {
  public waitingForResult: boolean = true;
  public phaseTwoCompletedSuccessfully: boolean;
  public shownPhaseTwoSolution: PhaseTwoModel = {} as PhaseTwoModel;
  public currentProfessorFilter: ProfessorModel = {} as ProfessorModel;
  public resetFilter: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private subscriptions: Subscription[] = [];
  private originalPhaseTwoSolution: PhaseTwoModel = {} as PhaseTwoModel;

  constructor(
    public mainCommunicationService: MainCommunicationService,
    private phaseOneTwoService: PhaseOneTwoService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    // load solution for phase 2 with selected solution from phase 1
    this.subscriptions.push(
      this.mainCommunicationService
        .executeTask(
          this.mainCommunicationService.PHASE2,
          this.phaseOneTwoService.selectedPhaseOneSolution
        )
        .subscribe((response) => {
          if (response) {
            this.originalPhaseTwoSolution = response;
            this.shownPhaseTwoSolution = response;
            this.phaseTwoCompletedSuccessfully = this.originalPhaseTwoSolution.completedSuccessfully;
            this.waitingForResult = false;
          }
        })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Propagates selected professor to filter shown solution of phase 2.
   * 
   * If selected professor not valid or present it resets filter and shown solution.
   *
   * @param {ProfessorModel} selectedProfessor professor to filter phase 2 result by
   */
  public professorToFilterSelected(selectedProfessor: ProfessorModel) {
    // typeahead component will return null if no professor for input string found
    if (selectedProfessor) {
      this.currentProfessorFilter = selectedProfessor;
      
      const professorModules = selectedProfessor.modules;
      const professorTimeslotGroupModules = this.getTimeslotGroupsWithModulesHeldByProfessor(
        professorModules
      );
      const professorUnassignedModules = this.getUnassignedModulesHeldByProfessor(
        professorModules
      );

      this.shownPhaseTwoSolution = Object.assign(
        {},
        this.shownPhaseTwoSolution
      ); // Set as new object to not override values in previous object (because it possibly is originalPhaseTwoSolution)
      this.shownPhaseTwoSolution.groupedModules = professorTimeslotGroupModules,
      this.shownPhaseTwoSolution.unallocatedModules = professorUnassignedModules,
      this.shownPhaseTwoSolution.numberOfUnallocatedLectures = professorUnassignedModules.length;
      this.shownPhaseTwoSolution.hasUnallocatedLectures = professorUnassignedModules.length > 0;
    }
    // if an invalid value was selected, reset filters and shown solution to initial state after retrieval
    else {
      this.currentProfessorFilter = {} as ProfessorModel;
      this.resetFilter.next(true);
      this.shownPhaseTwoSolution = this.originalPhaseTwoSolution;
    }
  }

  /**
   * Filters modules of each TimeslotGroup of originalPhaseTwoSolution by professorModules
   *
   * @param {ModuleModel[]} professorModules modules of professor to filter by
   * @returns {PhaseTwoTimeslotGroupModuleModel[]} TimeslotGroups filtered by professorModules
   */
  private getTimeslotGroupsWithModulesHeldByProfessor(
    professorModules: ModuleModel[]
  ) : PhaseTwoTimeslotGroupModuleModel[] {
    const professorTimeslotGroupModules: PhaseTwoTimeslotGroupModuleModel[] = [];

    this.originalPhaseTwoSolution.groupedModules.forEach((tm) => {
      // check if for the TimeslotGroup in question the selected professor holds a module
      if (
        tm.modules.some((m) =>
          this.professorAssignedToModule(professorModules, m)
        )
      ) {
        const newTimeslotGroupModules = Object.assign({}, tm);
        // if so filter to leave the according modules on the TimeslotGroup for showing
        newTimeslotGroupModules.modules = newTimeslotGroupModules.modules.filter(
          (m) => this.professorAssignedToModule(professorModules, m)
        );
        professorTimeslotGroupModules.push(newTimeslotGroupModules);
      }
    });

    return professorTimeslotGroupModules;
  }

  /**
   * Filters unassigned modules of originalPhaseTwoSolution by professorModules
   *
   * @param {ModuleModel[]} professorModules modules of professor to filter by
   * @returns {ModuleModel[]} modules of professor that could not be assigned
   */
  private getUnassignedModulesHeldByProfessor(professorModules: ModuleModel[]): ModuleModel[] {
    const professorUnassignedModules:ModuleModel[] = [];
    this.originalPhaseTwoSolution.unallocatedModules.forEach((m) => {
      if (this.professorAssignedToModule(professorModules, m)) {
        professorUnassignedModules.push(m);
      }
    });
    return professorUnassignedModules;
  }

  /**
   * Checks professorModules for matching id of module (as it is uniquely assigned by database)
   * 
   * Reasoning for id comparison: 
   * In db professor is also stored on module (since only one can be assigned), but in JSON these "loops" can't be represented.
   * In our case it was more useful to keep the information stored on professor rather than module (since it is also set this way in professor-configuration)
   *
   * @param {ModuleModel[]} professorModules all modules of professor
   * @param {ModuleModel} module professor to check if matching
   * @returns {boolean} if matching module found
   */
  private professorAssignedToModule(
    professorModules: ModuleModel[],
    module: ModuleModel
  ): boolean {
    return professorModules.some((pm) => pm.id == module.id);
  }

  public resetProfessorFilter() {
    this.professorToFilterSelected(null);
    this.openSnackBar('Filter gelöscht', 'X');
  }

  public openExportDialog() {
    const dialogConfig: PDFExportConfigurations = {
      pdfName: '',
      showLegend: false,
      tableData: document.getElementById('week-calender'),
      moduls: null,
      legendeData: document.getElementById(
        'legend-week-calender'
      ),
    };

    this.dialog.open(HtmlToPdfDialogComponent, {
      width: '50%',
      maxHeight: '75%',
      data: dialogConfig,
    });
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }
}
