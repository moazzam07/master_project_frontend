import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhaseTwoPageComponent } from './phase-two-page/phase-two-page.component';

const routes: Routes = [{ path: '', component: PhaseTwoPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhaseTwoRoutingModule {}
