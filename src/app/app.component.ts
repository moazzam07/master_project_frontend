import { Component } from '@angular/core';
import { MainCommunicationService } from './shared/services/load-data/main-communication.service';
import { PhaseOneTwoService } from './shared/services/injection/phase-one-two-injectable.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MainCommunicationService, PhaseOneTwoService],
})
export class AppComponent {
  title = 'Stundenplanner';
}
