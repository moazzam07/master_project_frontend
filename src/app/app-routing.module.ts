import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
  },
  {
    path: 'professor-configuration',
    loadChildren: () =>
      import('./professor-configuration/professor-configuration.module').then(
        (m) => m.ProfessorConfigurationModule
      ),
  },
  {
    path: 'module-configuration',
    loadChildren: () =>
      import('./module-configuration/module-configuration.module').then(
        (m) => m.ModuleConfigurationModule
      ),
  },
  {
    path: 'phase-one',
    loadChildren: () =>
      import('./phase-one/phase-one.module').then((m) => m.PhaseOneModule),
  },
  {
    path: 'phase-two',
    loadChildren: () =>
      import('./phase-two/phase-two.module').then((m) => m.PhaseTwoModule),
  },
  {
    path: 'settings-configuration',
    loadChildren: () =>
      import('./settings-configuration/settings-configuration.module').then(
        (m) => m.SettingsConfigurationModule
      ),
  },
  {
    path: 'constraint-configuration',
    loadChildren: () =>
      import('./constraint-configuration/constraint-configuration.module').then(
        (m) => m.ConstraintConfigurationModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
