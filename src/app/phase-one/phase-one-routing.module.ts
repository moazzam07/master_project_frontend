import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhaseOnePageComponent } from './phase-one-page/phase-one-page.component';

const routes: Routes = [{ path: '', component: PhaseOnePageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhaseOneRoutingModule {}
