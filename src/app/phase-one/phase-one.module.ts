import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SharedModule } from '../shared/shared.module';
import { PhaseOnePageComponent } from './phase-one-page/phase-one-page.component';
import { PhaseOneRoutingModule } from './phase-one-routing.module';

@NgModule({
  declarations: [PhaseOnePageComponent],
  imports: [
    CommonModule,
    PhaseOneRoutingModule,
    SharedModule,
    MatButtonToggleModule,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
})
export class PhaseOneModule {}
