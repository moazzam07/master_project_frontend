import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PhaseOnePageComponent } from './phase-one-page.component';

describe('PhaseOnePageComponent', () => {
  let component: PhaseOnePageComponent;
  let fixture: ComponentFixture<PhaseOnePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhaseOnePageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseOnePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
