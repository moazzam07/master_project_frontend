import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PhaseOneReturnModel } from 'src/app/shared/models/models';
import { MainCommunicationService } from 'src/app/shared/services/load-data/main-communication.service';
import { PhaseOneSolutionModel } from './../../shared/models/models';
import { PhaseOneTwoService } from '../../shared/services/injection/phase-one-two-injectable.service';

@Component({
  selector: 'phase-one-page',
  templateUrl: './phase-one-page.component.html',
  styleUrls: ['./phase-one-page.component.scss'],
})
export class PhaseOnePageComponent implements OnInit, OnDestroy {
  private readonly NUMBER_OF_PROPOSED_SOLUTIONS = 5; // Number of solutions to be proposed in pagination element

  public phaseOneReturn: PhaseOneReturnModel;
  public selectedPhaseOneSolution: PhaseOneSolutionModel;
  public currentlyProposedSolutionIds: number[] = [];
  public selectedSolutionsId: string;
  public hasPreviousSolutions: boolean;
  public hasFollowingSolutions: boolean;
  public waitingForResult: boolean = true;

  @ViewChild(MatButtonToggleGroup)
  private buttonToggleGroup: MatButtonToggleGroup;
  private subscriptions: Subscription[] = [];
  private numberOfProposedSolutions: number;

  constructor(
    private mainCommunicationService: MainCommunicationService,
    private phaseOneTwoService: PhaseOneTwoService,
    private router: Router
  ) {}

  ngOnInit() {
    // load solutions from phase 1 as per stored settings in db
    this.subscriptions.push(
      this.mainCommunicationService
        .executeTask(this.mainCommunicationService.PHASE1)
        .subscribe((response) => {
          if (response) {
            this.phaseOneReturn = response;
            this.determinedSolutionsToPropose();
            if (
              this.phaseOneReturn.solutionStatus &&
              this.phaseOneReturn.solutionList?.length > 0
            ) {
              this.onPaginationEvent('0');
            }
            this.waitingForResult = false;
          }
        })
    );
  }

  /**
   * Prepares shown list of solutions & navigation elements depending on number of returned solutions & wanted proposed solutions.
   */
  private determinedSolutionsToPropose() {
    if (this.phaseOneReturn?.solutionList?.length > 0) {
      const numberOfTotalSolutions = this.phaseOneReturn.solutionList.length;

      // more solutions than should be proposed
      if (numberOfTotalSolutions > this.NUMBER_OF_PROPOSED_SOLUTIONS) {
        this.numberOfProposedSolutions = this.NUMBER_OF_PROPOSED_SOLUTIONS;
        this.hasFollowingSolutions = true;
      }

      // less solutions than should be proposed
      else {
        this.numberOfProposedSolutions = numberOfTotalSolutions;
        // hasFollowingSolutions already correctly false as per default
      }

      // add as many solutions to list as determined
      for (let i = 0; i < this.numberOfProposedSolutions; i++) {
        this.currentlyProposedSolutionIds.push(i);
      }
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Handles value passed on change from mat-button-toggle-group.
   *
   * @param {string} paginationValue string value from selected mat-button-toggle
   */
  public onPaginationEvent(paginationValue: string) {
    switch (paginationValue) {
      case '<':
        this.shiftProposedSolutionsIdsLeft();
        break;

      case '>':
        this.shiftProposedSolutionIdsRight();
        break;

      default:
        this.propagateSolutionSelected(paginationValue);
        break;
    }
  }

  /**
   * Replaces the highest id of the proposed solutions with the next lower id (that is not proposed yet).
   * Determines hasPreviousSolutions & hasFollowingSolutions.
   */
  private shiftProposedSolutionsIdsLeft() {
    const lowestId = this.currentlyProposedSolutionIds[0];
    const newLowestId = lowestId - 1;
    this.currentlyProposedSolutionIds.splice(
      this.numberOfProposedSolutions - 1, // cut last element
      1,
      newLowestId // replace with new found id
    );
    this.sortCurrentlyProposedSolutionsIds();

    this.hasPreviousSolutions = newLowestId > 0;
    this.hasFollowingSolutions = true;
    this.reselectLastSolutionId();
  }

  /**
   * Replaces the lowest id of the proposed solutions with the next higher id (that is not proposed yet).
   * Determines hasPreviousSolutions & hasFollowingSolutions.
   */
  private shiftProposedSolutionIdsRight() {
    const highestId = this.currentlyProposedSolutionIds[
      this.numberOfProposedSolutions - 1
    ];
    const newHighestId = highestId + 1;
    this.currentlyProposedSolutionIds.splice(0, 1, newHighestId); // cut first element and replace with found id
    this.sortCurrentlyProposedSolutionsIds();

    this.hasPreviousSolutions = true;
    this.hasFollowingSolutions =
      newHighestId < this.phaseOneReturn.solutionList.length - 1;
    this.reselectLastSolutionId();
  }

  /**
   * Sorts all ids in list currentlyProposedSolutionIds ascending to display them in logic order.
   */
  private sortCurrentlyProposedSolutionsIds() {
    this.currentlyProposedSolutionIds.sort((a, b) => a - b);
  }

  /**
   * Sets last select solution id as value of mat-button-toggle-group (again).
   * This is done as otherwise pagination toggles would be displayed as selected and could therefore not be pressed again.
   */
  private reselectLastSolutionId() {
    this.buttonToggleGroup.value = this.selectedSolutionsId;
  }

  /**
   * Checks if paginationValue is numeric.
   *
   * If so: Selects the corresponding solution and id internally
   *
   * @param {string} paginationValue string value from selected mat-button-toggle
   */
  private propagateSolutionSelected(paginationValue: string) {
    // convert string to number and check if it returned one.
    var possibleNumber: number = parseInt(paginationValue);
    if (
      possibleNumber >= 0 &&
      possibleNumber < this.phaseOneReturn.solutionList.length
    ) {
      this.selectedPhaseOneSolution = this.phaseOneReturn.solutionList[
        possibleNumber
      ];
      this.selectedSolutionsId = paginationValue;
    }
  }

  /**
   * Passes currently selected phase 1 solution to transition service.
   * Navigates to phase-two page.
   */
  public passCurrentSelectionToPhaseTwo() {
    this.phaseOneTwoService.selectedPhaseOneSolution = this.selectedPhaseOneSolution.timeSlotGroups;
    this.router.navigate(['phase-two']);
  }

  /**
   * Builds string value of current select solution.
   *
   * @returns {string} string value of currently selected solution
   */
  public buildInformationString(): string {
    var solutionId: number = parseInt(this.selectedSolutionsId) + 1;

    return this.phaseOneReturn?.solutionList?.length
      ? `Lösung ${solutionId} von ${this.phaseOneReturn?.solutionList?.length}`
      : '';
  }
}
