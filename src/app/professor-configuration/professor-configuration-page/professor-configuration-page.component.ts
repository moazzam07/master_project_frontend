import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { ModuleSelectionComponent } from 'src/app/shared/components/module-selection/module-selection.component';
import { TypeaheadComponent } from 'src/app/shared/components/typeahead/typeahead.component';
import { MainCommunicationService } from 'src/app/shared/services/load-data/main-communication.service';
import {
  ModuleModel,
  ProfessorConfigurationWeekCalenderTransitionModel,
} from '../../shared/models/models';
import { ProfessorModel } from './../../shared/models/models';

@Component({
  selector: 'professor-configuration-page',
  templateUrl: './professor-configuration-page.component.html',
  styleUrls: ['./professor-configuration-page.component.scss'],
})
export class ProfessorConfigurationPageComponent implements OnInit, OnDestroy {
  public selectedProfessor: ProfessorModel = {
    adjacencyCostMultiplier: 0,
  } as ProfessorModel;
  public selectedProfessorAvailability: ProfessorConfigurationWeekCalenderTransitionModel = {} as ProfessorConfigurationWeekCalenderTransitionModel; // internally saved again to enable abort

  public availabilityExpanded: boolean;
  public moduleExpanded: boolean;
  public settingsExpanded: boolean;

  public professorSettingsConfigurationGroup: FormGroup;
  public adjacencyCostMultiplierControl = new FormControl(
    this.selectedProfessor.adjacencyCostMultiplier,
    [Validators.min(0), Validators.max(100)]
  );

  @ViewChild(TypeaheadComponent)
  private typeaheadComponent: TypeaheadComponent;
  @ViewChild(ModuleSelectionComponent)
  private moduleSelectionComponent: ModuleSelectionComponent;
  private subscriptions: Subscription[] = [];
  private selectedModules: ModuleModel[]; // internally saved again to enable abort

  constructor(
    public mainCommunicationService: MainCommunicationService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {
    this.professorSettingsConfigurationGroup = this.formBuilder.group({
      adjacencyCostMultiplier: this.adjacencyCostMultiplierControl,
    });
  }

  ngOnInit() {
    // add change listener to FormGroup of professor settings
    this.subscriptions.push(
      this.professorSettingsConfigurationGroup.valueChanges.subscribe(
        (data: ProfessorModel) => {
          if (data && this.professorSettingsConfigurationGroup.valid) {
            this.selectedProfessor.adjacencyCostMultiplier =
              data.adjacencyCostMultiplier;
          }
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Saves currently selected professor and notifies user on success.
   */
  public saveProfessor() {
    this.selectedProfessor.modules = this.selectedModules;

    this.subscriptions.push(
      this.mainCommunicationService
        .updateData(
          this.mainCommunicationService.PROFESSORS,
          this.selectedProfessor
        )
        .subscribe((data) => {
          if (data) {
            this.typeaheadComponent.reload();
            this.moduleSelectionComponent.reload();
            this.openSnackBar('Professor gespeichert!', 'X');
          }
        })
    );
  }

  /**
   * Propagate professor passed from typeahead to page component
   *
   * @param {ProfessorModel} selectedProfessor professor to propagate
   */
  public professorSelected(selectedProfessor: ProfessorModel) {
    // collapse all accordions elements to reset view and hide content changes of visible components
    this.availabilityExpanded = false;
    this.moduleExpanded = false;
    this.settingsExpanded = false;

    // typeahead component will return null if no professor for input string found
    if (selectedProfessor) {
      this.selectedProfessor = selectedProfessor;
      this.selectedModules = selectedProfessor.modules;

      this.selectedProfessorAvailability = {
        availableTimeslots: this.selectedProfessor.availableTimeslots,
        possibleTimeslots: this.selectedProfessor.possibleTimeslots,
        unavailableTimeslots: this.selectedProfessor.unavailableTimeslots,
      };

      this.adjacencyCostMultiplierControl.setValue(
        selectedProfessor.adjacencyCostMultiplier
      );
    } else {
      this.selectedProfessor = {} as ProfessorModel;
      this.selectedModules = null;
      this.selectedProfessorAvailability = null;
      this.adjacencyCostMultiplierControl.setValue(null);
    }
  }

  /**
   * Propagate changes in week-calender to availabilty of selected professor.
   *
   * @param {ProfessorConfigurationWeekCalenderTransitionModel} changedAvailability changed availability in week-calender
   */
  public availabilityChanged(
    changedAvailability: ProfessorConfigurationWeekCalenderTransitionModel
  ) {
    if (changedAvailability) {
      this.selectedProfessor.availableTimeslots =
        changedAvailability.availableTimeslots;
      this.selectedProfessor.possibleTimeslots =
        changedAvailability.possibleTimeslots;
      this.selectedProfessor.unavailableTimeslots =
        changedAvailability.unavailableTimeslots;
    }
  }

  /**
   * Propagate change of selection in module-selection to selected professor.
   *
   * @param {ModuleModel[]} selectedModules changed modules in module-selection
   */
  public moduleSelectionChanged(selectedModules: ModuleModel[]) {
    if (selectedModules) {
      this.selectedModules = selectedModules;
    }
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }
}
