import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { SharedModule } from '../shared/shared.module';
import { ProfessorConfigurationPageComponent } from './professor-configuration-page/professor-configuration-page.component';
import { ProfessorConfigurationRoutingModule } from './professor-configuration-routing.module';

@NgModule({
  declarations: [ProfessorConfigurationPageComponent],
  imports: [
    CommonModule,
    ProfessorConfigurationRoutingModule,
    SharedModule,
    MatExpansionModule,
    MatGridListModule,
    MatButtonModule
  ],
})
export class ProfessorConfigurationModule {}
