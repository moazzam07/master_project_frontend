import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfessorConfigurationPageComponent } from './professor-configuration-page/professor-configuration-page.component';

const routes: Routes = [
  { path: '', component: ProfessorConfigurationPageComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfessorConfigurationRoutingModule {}
