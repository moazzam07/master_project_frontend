import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CsvImportDialogComponent } from 'src/app/shared/components/csv-import-dialog/csv-import-dialog.component';
import { TypeaheadComponent } from 'src/app/shared/components/typeahead/typeahead.component';
import { ModuleModel } from 'src/app/shared/models/models';
import { MainCommunicationService } from 'src/app/shared/services/load-data/main-communication.service';

@Component({
  selector: 'module-configuration-page',
  templateUrl: './module-configuration-page.component.html',
  styleUrls: ['./module-configuration-page.component.scss'],
})
export class ModuleConfigurationPageComponent implements OnDestroy {
  public selectedModule: ModuleModel = {} as ModuleModel;
  public selectedModuleValid = new BehaviorSubject<boolean>(false);;

  @ViewChild(TypeaheadComponent)
  private typeaheadComponent: TypeaheadComponent;
  private subscriptions: Subscription[] = [];

  constructor(
    public mainCommunicationService: MainCommunicationService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  public saveModule() {
    if (this.selectedModuleValid.value) {
      this.saveModuleInternal(this.selectedModule);
    }
  }

  /**
   * Saves the passed in module to the db.
   *
   * On successful update:
   * - Reloads module selection typeahead to receive updated values from db.
   * - Notifies user accordingly.
   *
   * @param {ModuleModel} module module to save
   */
  private saveModuleInternal(module: ModuleModel) {
    this.subscriptions.push(
      this.mainCommunicationService
        .updateData(this.mainCommunicationService.MODULE, module)
        .subscribe((response) => {
          if (response) {
            this.typeaheadComponent.reload();
            this.openSnackBar('Modul gespeichert!', 'X');
          }
        })
    );
  }

  /**
   * Opens Dialog to import modules from CSV.
   * Registers Listener to be triggered when dialog closes to save returned modules.
   */
  public openCSVDialog() {
    this.dialog
      .open(CsvImportDialogComponent, {
        width: '70%',
        maxHeight: '75%',
      })
      .afterClosed()
      .subscribe((results: ModuleModel[]) => {
        if (results) {
          results.forEach((result) => {
            this.saveModuleInternal(result);
          });
        }
      });
  }

  /**
   * Propagates module passed from typeahead to page component.
   *
   * @param {ModuleModel} selectedModule module to propagate
   */
  public moduleSelected(selectedModule: ModuleModel) {
    // typeahead component will return null if no module for input string found
    if (selectedModule) {
      this.selectedModule = selectedModule;
      this.selectedModuleValid.next(false); // only valid after validation done atleast once (some value was changed by user, no save necessary otherwise)
    } else {
      this.selectedModule = {} as ModuleModel;
      this.selectedModuleValid.next(false);
    }
  }

  /**
   * Propagates module changes passed from module-configuration component.
   *
   * @param {ModuleModel} adaptedModule module with changes to propagate
   */
  public moduleConfigurationChanged(adaptedModule: ModuleModel) {
    // module configuration component will return null if inputs not valid (or no module selected)
    if (adaptedModule) {
      this.selectedModule = adaptedModule; // changes in module-configuration component will be stored on individual object to enable abort
      this.selectedModuleValid.next(true);
    } else {
      this.selectedModuleValid.next(false);
    }
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: 'mb-5',
    });
  }
}
