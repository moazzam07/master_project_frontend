import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModuleConfigurationPageComponent } from './module-configuration-page.component';

describe('ModuleConfigurationPageComponent', () => {
  let component: ModuleConfigurationPageComponent;
  let fixture: ComponentFixture<ModuleConfigurationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModuleConfigurationPageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleConfigurationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
