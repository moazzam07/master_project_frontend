import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../shared/shared.module';
import { ModuleConfigurationPageComponent } from './module-configuration-page/module-configuration-page.component';
import { ModuleConfigurationRoutingModule } from './module-configuration-routing.module';

@NgModule({
  declarations: [ModuleConfigurationPageComponent],
  imports: [
    CommonModule,
    ModuleConfigurationRoutingModule,
    SharedModule,
    MatButtonModule,
  ],
})
export class ModuleConfigurationModule {}
