import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleConfigurationPageComponent } from './module-configuration-page/module-configuration-page.component';

const routes: Routes = [
  { path: '', component: ModuleConfigurationPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModuleConfigurationRoutingModule {}
