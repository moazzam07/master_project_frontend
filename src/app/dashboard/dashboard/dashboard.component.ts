import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { InfoErrorDialogComponent } from 'src/app/shared/components/info-error-dialog/info-error-dialog.component';
import { InfoErrorDialogConfiguration } from 'src/app/shared/models/models';
import { MetricsService } from '../../shared/services/load-data/metrics.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public profAmount: number;
  public profNames: string[];
  public subjectAmount: number;
  public subjectNames: string[];
  public unassignedSubjectNames: string[];
  public subjectUnassigned: number;

  constructor(
    private metricsService: MetricsService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  /**
   * Executs all REST-Calls initially, which are needed for the dashboard.
   */
  ngOnInit() {
    this.metricsService.getProfAmount();
    this.metricsService.getProfNames();
    this.metricsService.getSubjectAmount();
    this.metricsService.getSubjectNames();
    this.metricsService.getUnassignedSubjectNames();
    this.metricsService.getSubjectUnassigned();
    this.getData();
  }

  /**
   * Subscribes to all important data.
   */
  private getData() {
    this.metricsService.profAmountData$.subscribe((data) => {
      if (data) {
        this.profAmount = data;
      }
    });
    this.metricsService.profNamesData$.subscribe((data) => {
      if (data) {
        this.profNames = data;
      }
    });
    this.metricsService.subjectAmountData$.subscribe((data) => {
      if (data) {
        this.subjectAmount = data;
      }
    });
    this.metricsService.subjectNamesData$.subscribe((data) => {
      if (data) {
        this.subjectNames = data;
      }
    });
    this.metricsService.unassignedSubjectNamesData$.subscribe((data) => {
      if (data) {
        this.unassignedSubjectNames = data;
      }
    });
    this.metricsService.subjectUnassignedData$.subscribe((data) => {
      if (data) {
        this.subjectUnassigned = data;
      } else if (data == 0) {
        this.subjectUnassigned = data;
      }
    });
  }

  /**
   * Navigates to phase 1, only if all modules are assigned to one professor.
   * 
   * If not: An info dialog will be opened.
   */
  public startPhaseOne() {
    if (this.subjectUnassigned > 0 && this.subjectUnassigned !== 999) {
      this.openUnssignedModuleInfoDialog();
    } else if(this.subjectAmount === undefined || this.profAmount === undefined) {
      this.openNoModuleInfoDialog();
    } else {
      this.router.navigate(['phase-one']);

    }
  }

  private openUnssignedModuleInfoDialog() {
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText:
        'Bitte weisen Sie jedem Modul einen Professoren zu, bevor Sie die erste Phase starten!',
      buttonText: ['Schließen'],
      header: 'Informationen',
    };

    this.dialog.open(InfoErrorDialogComponent, {
      width: '50%',
      maxHeight: '75%',
      data: dialogConfig,
    });
  }
  private openNoModuleInfoDialog() {
    const dialogConfig: InfoErrorDialogConfiguration = {
      isError: false,
      displayText:
        'Bitte fügen Sie Professoren und Module hinzu, bevor Sie die erste Phase starten!',
      buttonText: ['Schließen'],
      header: 'Informationen',
    };

    this.dialog.open(InfoErrorDialogComponent, {
      width: '50%',
      maxHeight: '75%',
      data: dialogConfig,
    });
  }
}
